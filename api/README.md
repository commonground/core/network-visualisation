# Common Ground Network Hasura API

This project uses Hasura GraphQL backend with PostgreSQL.

[Hasura](https://hasura.io/) is instant realtime GraphQL on Postgres. For local development use docker compose file provided by Hasura. For more information see [official documentation](https://docs.hasura.io/1.0/graphql/manual/getting-started/docker-simple.html)

## Development

The frontend of Common Ground Networks (CGN) Visualisation uses D3.js force directed graph. The database structure is identical to structure required by D3. The front-end requires 2 tables to be created on Hasura instance: nodes and links. The tables are created automatically using Hasura migration scripts which can be found in the migration folder.

During development you can use Minikube with Skaffold and Helm or docker-compose to run an instance of Hasura. For more information about Hasura see [documenation website](https://docs.hasura.io/1.0/graphql/manual/getting-started/docker-simple.html)

## Running locally using Minikube, Skaffold and Helm

This is advised approach. For all steps see [main readme file](../README.md) in the root of the project.

## Running locally using docker-compose

We use Hasura docker image that supports table migrations. The migration scripts Hasura will use during intialization are in migration folder. For more information about migrations [see documentation](https://docs.hasura.io/1.0/graphql/manual/migrations/existing-project.html).

More information about applying Hasura migration scripts in the container is [provided on this page](https://docs.hasura.io/1.0/graphql/manual/migrations/auto-apply-migrations.html).

To start - stop / remove Hasura with required tables:

```bash
  # run docker-compose in detached mode in the api folder
  docker-compose up -d

  # validate instances are running in Docker
  # you should see 2 images running:
  #   1. hasura/graphql-engine:v1.0.0-alpha38
  #   2. postgres
  docker ps

  # to shut down Hasura api
  docker-compose down

  # if you want to remove volumes
  # locate volume
  docker volume ls

  # remove volume
  docker volume rm <volume name>

```

# Required PostgreSQL tables

This project uses 3 tables in the database (configuration, nodes and links). Hasura engine creates these tables using migration scripts and assings user right. There are two roles: admin (can do everything) and user (can only view).

- You can validate that tables, required by CG Network front-end, are created using Hasura interface. Depending on the approach hasura interface can be reached:

  - Skaffold approach: http://hasura.cgn.minikube:30443
  - Docker compose approach: http://localhost:8080

- Go to tab Data in Hasura interface
- Confirm that tables `configuration`,`nodes` and `links` are created with the following fields

  - **configuration**:

    - `column`: type, `type`: Text
    - `column`: createdAt, `type`: Timestamp, `default_value`: now()
    - `column`: dataB64, `type`: Text, `nullable`: true

  - **nodes**:

    - `column`: id, `type`: Text, `unique`: true, `PRIMARY KEY`
    - `column`: createdAt, `type`: Timestamp, `default_value`: now()
    - `column`: type, `type`: Text
    - `column`: label, `type`: Text
    - `column`: name, `type`: Text, `nullable`: true
    - `column`: dataB64, `type`: Text, `nullable`: true

  - **links**:
    - `column`: id, `type`: Text, `unique`: true, `PRIMARY KEY`
    - `column`: createdAt, `type`: Timestamp, `default_value`: now()
    - `column`: source, `type`: Text
    - `column`: target, `type`: Text
    - `column`: dataB64, `type`: Text, `nullable`: true

- Confirm that user has select rights on all 3 tables (see permissions tab)
