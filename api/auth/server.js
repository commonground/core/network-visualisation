/**
 * Common Ground Networks basic authentication server
 * with NodeJS and JsonWebToken.
 * v.0.0.1
 */

// express server
const express = require('express')
const bodyParser = require("body-parser")
const serverPort = process.env.API_AUTH_PORT || 5050
// admin username and password
const username = process.env.API_ADMIN_NAME || 'admin'
const password = process.env.API_ADMIN_PASS || 'password'
const admin={
  username,
  password
}
//private key
const privateKey = process.env.API_AUTH_KEY || '12aeasd123423srsfdds32423412890'

//json web token
const jwt = require('jsonwebtoken')
//token options
const jwtOptions={
  algorithm: 'HS512',
  expiresIn: '1h',
  issuer: 'network.commonground.nl'
}

// intialize express
const app = express()
app.use(bodyParser.json())

//-----------------------------------
// MIDDLEWARE: HANDLE OPTIONS request
//-----------------------------------
app.use((req,res,next)=>{
  //NOTE CORS should be adapted
  //allow requests only from 'http://ui.cgn.minikube:30080'
  //res.setHeader('Access-Control-Allow-Origin','http://ui.cgn.minikube:30080')
  //allow request from any origin
  res.setHeader('Access-Control-Allow-Origin','*')
  res.setHeader(
    'Access-Control-Allow-Methods','OPTIONS, GET, POST'
  )
  res.setHeader(
    'Access-Control-Allow-Headers', 'Content-Type,Authorization'
  )
  if (req.method==='OPTIONS'){
    // console.log("Options method allowed")
    return res.sendStatus(200)
  }
  next()
})


//--------------------------------------
// ROUTE - ROOT
//--------------------------------------
/**
 * Unprotected root page
 */
app.get('/',(req,res)=>{
  res.send(`
    <h2>Yes, it works!</h2>
    <p>This is nodejs authentication server of CGN.</p>
    <!--<ul>
      <li><a href="/api">Unprotected api point</a></li>
      <li><a href="/sign/user">JWT sign token as user</a></li>
      <li><a href="/signin">JWT signin as admin. Click to read more.</a></li>
      <li><a href="/validate">Validate received token</a></li>
      <li><a href="/api/user">Test api point for the user role</a></li>
      <li><a href="/api/admin">Test api point for admin role</a></li>
    </ul>-->
  `)
})

/**
 * Unprotected api point
 */
app.get('/api',(req,res)=>{
  res.json({
    message: "You are hitting UNPROTECTED api route! Try /api/user or /api/admin to test protected route."
  })
})

app.get('/api/user',validateToken,(req,res)=>{
  // user object is added to request object by validateToken
  // here we extract roles
  const {roles} = req.user

  if (roles.indexOf('user')>-1){
    console.log('/api/user...roles...', roles, '...OK')
    res.json({
      message: "Yes! You are alowed to access USER data!"
    })
  } else {
    console.log('/api/user...roles...', roles, '...403')
    res.status(403).json({
      message: "Huh! It seems that your JWT is missing USER role. Lame :-("
    })
  }
})

app.get('/api/admin',validateToken, (req,res)=>{
  // user object is added to request object by validateToken
  // here we extract roles
  const {roles} = req.user

  if (roles.indexOf('admin')>-1){
    console.log('/api/admin...roles...', roles, '...OK')
    res.json({
      message: "Yes! You are alowed to access ADMIN data!"
    })
  } else {
    console.log('/api/admin...roles...', roles, '...403')
    res.status(403).json({
      message: "Huh! It seems that your JWT is missing ADMIN role. Lame :-("
    })
  }
})

//-----------------------------------------------
// SIGN USER TOKEN
//-----------------------------------------------
app.get('/sign/user',(req,res)=>{
  res.send(`
    <h1> Sign user </h1>
    <p>You can create a user JWT using POST method to this point and providing
    user profile object, like this
    <pre>
    {
      "name": "user 1",
      "roles": ["user"]
    }
    </pre>
    </p>
    <p>
    The server will response with JWT like this
    <pre>
    {
      "message": "Here is the token for USER",
      "access_token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNTUyMzg4OTI2MTU4LCJuYW1lIjoidXNlciAxIiwicm9sZXMiOlsidXNlciJdfSwiaWF0IjoxNTUyMzg4OTI2LCJleHAiOjE1NTIzODg5NTYsImlzcyI6Im5ldHdvcmsuY29tbW9uZ3JvdW5kIn0.FwwhggpDX8q8QykKKGvRxld0xnzeNi9nRxuBRC2n0A2M_eGQrj2XUY-J00b-Tey8NQd9eZnss-as3sFcxLvfJw"
    }
    </pre>
    </p>
  `)
})

app.post('/sign/user',(req,res)=>{
  // user object with props
  if (!req.body.roles){
    console.log("/sign/user...BAD REQUEST...400")
    res.status(400).send("Bad request: roles array is required in the user profile object")
  }
  // user object with props
  let user = req.body
  if (user){
    const token = signToken(user)
    console.log("/sign/user...", user,"...OK")
    res.json({
      message: "Here is the token for USER",
      access_token: token
    })
  } else {
    console.log("/sign/user...FAILED...500")
    res.send(500)
  }
})

//-----------------------------------------------
// SIGNIN FOR ADMIN TOKEN
//-----------------------------------------------
app.get('/signin',(req,res)=>{
  res.send(`
    <h1>Signin for the admin token</h1>
    <p>Use POST method to this route to signin as admin. The user object you need
    to send expects user profile with at least username, password and array of roles,
    like this.
    <pre>
    {
      "username": "admin",
      "password": "password",
      "roles": ["user", "admin"]
    }
    </pre>
    </p>
    <p>
      Authentication server will remove password from user object and return access_token, like this
      <pre>
      {
        "message": "Here is the token for ADMIN,
        "access_token": "JWT-Token"
      }
      </pre>
      The token with contain user profile in it with provided roles. No role validation is perfomed
      by auth server becuse for admin signin in all roles are accepted. You do need to provide
      at least user and admin roles if you want to use network application to its full extent.
      To view JWT content use <a href="https://jwt.io/">JWT website</a>.
    </p>
  `)
})

app.post('/signin',(req,res)=>{
  // user object with props
  // console.log("/signin...body", req.body)
  if (!req.body.username){
    console.log("/signin...BAD REQUEST...400")
    res.status(400).json({
      code: 400,
      message: "Bad request: username is required in the user profile object"
    })
    return
  }
  if (!req.body.password){
    console.log("/signin...BAD REQUEST...400")
    res.status(400).json({
      code: 400,
      message: "Bad request: password is required in the user profile object"
    })
    return
  }
  let user = req.body
  // authenticate
  if (authenticateUser(user)===false){
    console.log("/signin...403")
    res.status(403).json({
      code: 403,
      message: "Forbidden! Username or password incorrect."
    })
    return
  }
  //remove password
  delete user.password
  console.log("admin info", user)
  if (user){
    // sign jwt
    try{
      const token = signToken(user)
      res.json({
        message: "Here is the token for ADMIN",
        access_token: token
      })
    }catch(e){
      console.error("Failed to sign token: ", e)
      res.status(500).send("Failed to sign token")
    }
  } else {
    console.log("/signin...user profile missing...500")
    res.status(500).send("User profile object missing")
  }
})

//-----------------------------------------------
// HASURA WEB HOOK
// Each hasura request is validated using this
// endpoint 'hook'. If validation is successful
// we return hasura user variables as json payload
// if not validateHasuraToken function
// returns 401. Hasura recognize 2 states:
// 200 or 401. All other states will cause
// hasura api to return 500 to frontend.
//-----------------------------------------------
app.get('/hasura',validateHasuraToken,(req,res)=>{
  // user object is added to request object by validateToken
  // here we extract roles
  const {roles} = req.user

  if (roles.indexOf('admin')>-1){
    console.log('/hasura...admin...', roles, '...OK')
    // Return appropriate response to Hasura
    var hasuraVariables = {
      'X-Hasura-User-Id': '1',
      'X-Hasura-Role': 'admin',
      'X-Hasura-Is-Owner': 'true'
    };
    res.json(hasuraVariables);
  } else if (roles.indexOf('user')>-1){
    console.log('/hasura...user...', roles, '...OK')
    // Return appropriate response to Hasura
    var hasuraVariables = {
      'X-Hasura-User-Id': '1',
      'X-Hasura-Role': 'user',
      'X-Hasura-Is-Owner': 'true'
    };
    res.json(hasuraVariables);
  } else {
    console.log('/hasura...jwt missing roles...', roles, '...401')
    res.status(401).json({
      message: "Uh! It seems that your JWT is missing a role. Lame :-("
    })
  }
})

/**
 * Validates request placed by hasura engine
 * it recognized only 2 states: 401 or 200
 * @param {Express.Request} req express request object
 * @param {Express.Response} res  express respose object
 * @param {Function} next express next function
 */
function validateHasuraToken(req, res, next){
  try{
    //get authorization header
    const bearerHeader =  req.headers['authorization']
    // if token not provided
    if (typeof bearerHeader === "undefined"){
      console.log('/hasura...bearer token missing...401')
      //hasura auth hook requires 401
      return res.status(401).json({
        message:'Not allowed without bearer token'
      })
      //return false
    }
    //if authorisation header provided
    const bearer = bearerHeader.split(" ")
    let token = bearer[1]
    if (!token){
      console.log('/hasura...bearer token missing...401')
      return res.status(401).json({
        message:'Not allowed without bearer token'
      })
    }
    //if token value provided
    //verify token
    const data = jwt.verify(token, privateKey)
    //attach veryfied token to request
    req.token = token
    req.user = data.user
    //request can continue
    next()
  }catch(e){
    console.log('/hasura...invalid token...401')
    return res.status(401).json({
      message:`Invalid token provided: ${e}`
    })
  }
}


/**
 * Authenticate admin user
 * @param {Object} user user profile object
 * @param {String} user.username user profile object
 * @param {String} user.password user profile object
 * @param {Array} user.roles = ['user', 'admin'] array of user roles
 */
function authenticateUser(user){
  if (user.username!==admin.username){
    console.log("Wrong username...", user.username, admin.username)
    return false
  }
  if (user.password!==admin.password){
    console.log("Wrong password...", user.password, admin.password)
    return false
  }
  return true
}

/**
 * Create signed token using jsonwebtoken library.
 * @param {Object} user
 */
function signToken(user){
  //add random userid
  user['id'] = new Date().getTime()
  const data={
    user
  }
  const token = jwt.sign(data, privateKey, jwtOptions)
  return token
}

/**
 * Validate if request has JWT token in the header and
 * if token is not expired
 * @param {Express.Request} req express request object
 * @param {Express.Response} res  express respose object
 * @param {Function} next express next function
 */
function validateToken(req, res, next){
  try{
    //get authorization header
    const bearerHeader =  req.headers['authorization']
    // if token not provided
    if (typeof bearerHeader === "undefined"){
      console.log('validateToken...missing bearer token...403')
      return res.status(403).json({
        message:'Not allowed without bearer token'
      })
      //return false
    }
    //if authorisation header provided
    const bearer = bearerHeader.split(" ")
    let token = bearer[1]
    if (!token){
      console.log('validateToken...missing bearer token...403')
      return res.status(403).json({
        message:'Not allowed without bearer token'
      })
      //return false
    }
    //if token value provided
    //verify token
    const data = jwt.verify(token, privateKey)
    //attach verifyed token to request
    req.token = token
    req.user = data.user
    //request can continue
    next()
  }catch(e){
    console.log('validateToken...invalid token...403')
    return res.status(403).json({
      message:`Invalid token provided: ${e}`
    })
    //return false
  }
}

//-------------------------------------
// VALIDATE TOKEN
app.get('/validate',(req,res)=>{
  res.send(`
    <h1>Validate token</h1>
    <p>You can valide your JWT token, received from sign/user route by using POST method to this point
      and providing JWT in the header of the request as <br>
      Authorization: Bearer {JWT_access_token}
    </p>
    <p>
      If the token is valid, you can then try verifying user roles on following points:
      <br>
      api/user
      <br>
      api/admin
    </p>
  `)
})

app.post('/validate',validateToken, (req,res)=>{
  res.json({
    message: "Your token is valid!",
    token: req.token,
    user: req.user
  })
})


console.log("NODE_ENV...", process.env.NODE_ENV)
console.log("API_AUTH_PORT...", process.env.API_AUTH_PORT)
console.log("API_ADMIN_NAME...", process.env.API_ADMIN_NAME)
console.log("API_ADMIN_PASS...", process.env.API_ADMIN_PASS)
console.log("API_AUTH_KEY...", process.env.API_AUTH_KEY)

app.listen(serverPort,()=>console.log(`Auth server on port ${serverPort}`))