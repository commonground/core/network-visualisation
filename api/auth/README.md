# Common Ground Networks auth... server

This server provides basic authentication and JWT validation
functionality to Common Ground Networks application.

For more information about the server features start the server and open the homepage.

## Development

To install all requirements run `npm install`
To start server localy run `npm run dev`.
The server will be at http://localhost:5050

## Create Docker container

```bash
  docker build -t cgn-oauth2:0.0.1 .
```

## Get into node-alpine container

```bash
  docker exec -it <containerid> /bin/ash
```

## View logs (from console.log in nodejs app)

```bash
  docker logs <containerid>
```
