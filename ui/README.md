# Common Ground Network Visualization Front-end

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Development

You can run this project on Minikube with Helm and Skaffold during development. See README file in the root of the project. If you want to run api and frontend separately see next chapter.

### Runing standalone

- First start api using docker compose in the api folder

```bash
  # navigate to api folder from ui folder
  cd ../api
  # start backend api using docker-compose
  docker-compose up -d

  # to stop backend api
  # docker-compose down
```

- Second start ui in dev mode

```bash
  # navigate back to ui folder from api
  cd ../ui
  # start ui in dev mode (api assumed on loalhost:8080)
  npm run dev
  # to stop use Ctrl/Cmd + C
```

Beside dev mode the following npm scripts are available.

```bash

  ## install npm dependencies
  npm install

  ## start project and open nieuw browser tab
  ## Note! assumes local Hasura instance, see api folder
  npm start

  ## start project without opening browser
  ## Note: assumes Hasura api on http://localhost:8080/v1alpha1/graphql
  ## If you need different api point update npm dev script
  npm run dev

  ## start project using the test Hasura backend on minikube
  npm run dev:minikube

  ## start project using the test Hasura backend on Heroku
  ## this is temporary approach and will be soon discontinued
  npm run dev:heroku

  ## run tests with Jest and Enzyme
  npm test

  ## default build without PUBLIC_URL param
  npm run build

  ## build the front-end for Gitlab pages
  ## Note: this script uses following urls
  ## public_url: https://commonground.gitlab.io/cg-network-visualisation
  ## hasura api: https://cg-network-d3v5.herokuapp.com/v1alpha1/graphql
  npm run build:pages

```

### Docker

The cg-network-ui can be build as Docker container using Dockerfile. The Dockerfile uses node:10.15-alpine to build ui and nginx:alpine image as final container. With this approach the ui container size is reduced to minimum.

The steps to build cg-network-ui with Docker

```bash
  # create docker image cg-network-ui:latest
  docker build -t cg-network-ui .

  # run container on port 8888
  # NOTE! if you run api hasura interface uses 8080
  docker run -d -p 8888:80 --name=nginx-cg-network-ui cg-network-ui

```

### Other React CRA scripts

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More about React CRA

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
