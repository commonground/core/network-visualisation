import React, { Component } from 'react'

import ForceDirectedGraph from '../../components/ForceDirectedGraph/ForceDirectedGraph'
import {
  toolboxButtonsUser,
  toolboxButtonsAdmin
} from '../../components/ForceDirectedGraph/Toolbox/toolboxButtons'
import AuthSvc from '../../components/AuthRoute/AuthSvc'

class NetworkPage extends Component {
  getToolboxItems = () => {
    let toolbox = {
      title: 'Common Ground Network',
      buttons: []
    }
    if (AuthSvc.tokenContainsUserRole('admin')) {
      toolbox.buttons = toolboxButtonsAdmin
    } else {
      toolbox.buttons = toolboxButtonsUser
    }
    return toolbox
  }

  onRedirectAction = action => {
    let { history } = this.props
    if (action.payload) {
      history.push(action.payload)
    } else {
      console.warn('redirect has no payload')
    }
  }

  render() {
    //debugger
    return (
      <ForceDirectedGraph
        toolbox={this.getToolboxItems()}
        onRedirect={this.onRedirectAction}
      />
    )
  }
}

export default NetworkPage
