import React from 'react'
import IconGhost from '../../styles/icons/ghost.svg'
import '../../components/AuthRoute/Page403.scss'
/**
 * Based on error pages idea from Greg Shuster
 * for more info see: https://dribbble.com/shots/3023730-Sketch-Error-Pages
 */
const PageNotFound = () => {
  return (
    <section className="Page403">
      <h1 className="Page403-title">404 Page not found</h1>
      <div className="Page403-image">
        <span>4</span>
        <img src={IconGhost} alt="not found" />
        <span>4</span>
      </div>
      <div className="Page403-message">
        Page not found. You might try <a href="/">home page</a>
      </div>
    </section>
  )
}

export default PageNotFound
