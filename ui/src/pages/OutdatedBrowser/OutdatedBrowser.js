import React from 'react'
import NotificationIcon from '../../styles/icons/notification_important.svg'
import './OutdatedBrowser.scss'

const OutdatedBrowser = () => {
  return (
    <div className="OutdatedBrowser">
      <h1>Browser not supported</h1>
      <img src={NotificationIcon} alt="notification" />
      <p>
        Unfortunately this browser does not support web
        technologies required by this application. You’ll need
        to try another browser. The application supports all
        major evergreen browsers: Chome, Firefox, Safari and
        Opera. For more information about popular browsers
        &nbsp;
        <a href="http://outdatedbrowser.com" target="_new">
          check this website
        </a>
        .
      </p>
    </div>
  )
}

export default OutdatedBrowser
