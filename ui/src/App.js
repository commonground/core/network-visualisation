import React, { Component } from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom'

import AuthRoute from './components/AuthRoute/AuthRoute'
import NetworkPage from './pages/NetworkPage/NetworkPage'
import OutdatedBrowser from './pages/OutdatedBrowser/OutdatedBrowser'
import PageNotFound from './pages/PageNotFound/PageNotFound'

import './App.scss'
import Header from './components/Header/Header'

class App extends Component {
  /**
   * Test if browser can create new Promise.
   * Older browsers like IE11 do not support
   * promises. In addition there are some flexbox
   * layout bugs. This app does not work
   * properly in IE11 so we show an outdated
   * browser message.
   */
  browserSupportsPromise = () => {
    try {
      new Promise((res, rej) => {
        res(true)
      })
      return true
    } catch (e) {
      console.warn('Promises not supported by the browser')
      return false
    }
  }
  render() {
    if (this.browserSupportsPromise() === true) {
      return (
        <article className="App">
          <Header />
          <BrowserRouter>
            <main className="App-main">
              <Switch>
                <AuthRoute
                  exact
                  path="/"
                  data-role="user"
                  component={NetworkPage}
                />
                <AuthRoute
                  exact
                  path="/admin"
                  data-role="admin"
                  component={NetworkPage}
                />
                <Route component={PageNotFound} />
              </Switch>
            </main>
          </BrowserRouter>
        </article>
      )
    } else {
      return (
        <div className="App">
          <Header />
          <div className="App-main">
            <OutdatedBrowser />
          </div>
        </div>
      )
    }
  }
}

export default App
