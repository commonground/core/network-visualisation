import React from 'react'
import IconImportant from '../../../styles/icons/notification_important.svg'
import './EmptyPlaceholder.scss'
/**
 * Placeholder for messages to make user aware
 * that his/her input is required
 * The styles are 'borrowed' from Page403
 */
const EmptyPlaceholder = props => {
  const { noIcon, className } = props
  let classes = 'EmptyPlaceholder'
  if (className) {
    classes = `EmptyPlaceholder ${className}`
  }
  let iconHtml = (
    <div className="EmptyPlaceholder-image">
      <img src={IconImportant} alt="important" />
    </div>
  )
  if (noIcon) {
    iconHtml = null
  }
  return (
    <section className={classes}>
      <h1 className="EmptyPlaceholder-title">{props.title}</h1>
      {iconHtml}
      <div className="EmptyPlaceholder-message">
        {props.message}
      </div>
    </section>
  )
}

export default EmptyPlaceholder
