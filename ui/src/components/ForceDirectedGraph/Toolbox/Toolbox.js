import React, { Component } from 'react'

import './Toolbox.scss'

import IconButton from './IconButton'

class Toolbox extends Component {
  onButtonClick = action => {
    this.props.onButtonClick(action)
  }
  getButtonHtml() {
    let { buttons } = this.props
    let btnHtml = buttons.map(btn => {
      //add onButtonClick method to icon
      btn['onIconBtnClick'] = this.onButtonClick.bind(
        this,
        btn.action
      )
      return <IconButton key={btn.action.type} {...btn} />
    })
    return btnHtml
  }
  render() {
    return (
      <nav className="Toolbox">
        <h3 className="Toolbox-message">{this.props.title}</h3>
        <div className="Toolbox-tools">
          {this.getButtonHtml()}
        </div>
      </nav>
    )
  }
}

export default Toolbox
