import IconResetZoom from '../../../styles/icons/baseline-zoom_out_map-24px.svg'
import IconReload from '../../../styles/icons/baseline-refresh-24px.svg'
import IconPlay from '../../../styles/icons/baseline-play_arrow-24px.svg'
import IconSettings from '../../../styles/icons/baseline-settings-20px.svg'
import IconEdit from '../../../styles/icons/baseline-edit-24px.svg'
import IconTune from '../../../styles/icons/baseline-tune-24px.svg'
import IconLogout from '../../../styles/icons/baseline-directions_run-24px.svg'

import * as actionType from '../Helpers/cg.types'

const toolboxButtonsAdmin = [
  {
    action: {
      type: actionType.RESTART_SIM
    },
    label: 'Apply force',
    icon: {
      src: IconPlay,
      alt: 'apply force'
    }
  },
  {
    action: {
      type: actionType.RELOAD_SIM
    },
    label: 'Reload network',
    icon: {
      src: IconReload,
      alt: 'reload network'
    }
  },
  {
    action: {
      type: actionType.RESET_ZOOM
    },
    label: 'Reset zoom',
    icon: {
      src: IconResetZoom,
      alt: 'reset zoom'
    }
  },
  {
    action: {
      type: actionType.SIM_SETTINGS
    },
    label: 'Simulation settings',
    icon: {
      src: IconTune,
      alt: 'simulation settings'
    }
  },
  {
    action: {
      type: actionType.APP_SETTINGS
    },
    label: 'Edit network settings',
    icon: {
      src: IconSettings,
      alt: 'network settings'
    }
  },
  {
    action: {
      type: actionType.LOG_OUT
    },
    label: 'Log out',
    icon: {
      src: IconLogout,
      alt: 'log out'
    }
  }
]

const toolboxButtonsUser = [
  {
    action: {
      type: actionType.RESTART_SIM
    },
    label: 'Apply force',
    icon: {
      src: IconPlay,
      alt: 'apply force'
    }
  },
  {
    action: {
      type: actionType.RELOAD_SIM
    },
    label: 'Reload network',
    icon: {
      src: IconReload,
      alt: 'reload network'
    }
  },
  {
    action: {
      type: actionType.RESET_ZOOM
    },
    label: 'Reset zoom',
    icon: {
      src: IconResetZoom,
      alt: 'reset zoom'
    }
  },
  {
    action: {
      type: actionType.URL_REDIRECT,
      payload: '/admin'
    },
    label: 'Edit network',
    icon: {
      src: IconEdit,
      alt: 'edit network'
    }
  }
]

export { toolboxButtonsAdmin, toolboxButtonsUser }
