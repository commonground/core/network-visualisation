import React from 'react'

import './IconButton.scss'
/**
 * Icon buttons.
 * @param {Object} props icon props
 * @param {Function} props.onIconBtnClick callback function on button click
 * @param {String} props.label mouseover label (button title prop)
 * @param {Object} props.icon icon info
 * @param {String} props.icon.src image source of icon
 * @param {String} props.icon.alt image source alt text
 */
const IconButton = props => {
  const {
    action,
    label,
    disabled,
    icon,
    onIconBtnClick
  } = props
  //console.log('IconButton...', action)
  return (
    <button
      className="IconButton"
      onClick={onIconBtnClick.bind(this, action)}
      title={label}
      disabled={disabled}>
      <img
        className="IconButton-img"
        src={icon.src}
        alt={icon.alt}
      />
    </button>
  )
}

export default IconButton
