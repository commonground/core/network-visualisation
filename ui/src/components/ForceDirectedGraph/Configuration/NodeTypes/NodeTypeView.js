import React, { Component } from 'react'

import IconButton from '../../Toolbox/IconButton'
import { viewNodeTypeButtons } from './toolboxButtons'

import './NodeTypeView.scss'

class NodeTypeView extends Component {
  state = null
  btns = []
  onButtonClick = action => {
    this.props.onButtonClick(action)
  }
  getNavButtons = id => {
    let navHtml = viewNodeTypeButtons.map(btn => {
      //new object
      let newBtn = {
        ...btn,
        action: {
          type: btn.action.type,
          payload: {
            id
          }
        },
        onIconBtnClick: this.onButtonClick
      }
      return <IconButton key={id} {...newBtn} />
    })
    return navHtml
  }
  getNodeTypesTableHeader() {
    return (
      <tr>
        <th>Node color</th>
        <th>Label</th>
        <th>Id</th>
        <th />
      </tr>
    )
  }
  getNodeTypesTableRow(nodeType) {
    const { id, color, label } = nodeType

    let innerHtml = (
      <tr key={id}>
        <td>
          <div className="NodeType-color">
            <div
              className="NodeType-color-sample"
              style={{ backgroundColor: color }}
              title="Color sample"
            />
            {color}
          </div>
        </td>
        <td>{label}</td>
        <td>{id}</td>
        <td className="NodeTypeView-nav">
          {this.getNavButtons(id)}
        </td>
      </tr>
    )
    return innerHtml
  }

  getNodeTypesTableBody() {
    const { nodeTypes } = this.props
    const keys = Object.keys(nodeTypes)
    let tblBodyHtml = []

    tblBodyHtml.push()

    keys.forEach(key => {
      let nodeType = nodeTypes[key]
      let tblRow = this.getNodeTypesTableRow(nodeType)
      tblBodyHtml.push(tblRow)
    })

    return tblBodyHtml
  }

  render() {
    return (
      <table className="NodeTypeView">
        <tbody>{this.getNodeTypesTableBody()}</tbody>
      </table>
    )
  }
}

export default NodeTypeView
