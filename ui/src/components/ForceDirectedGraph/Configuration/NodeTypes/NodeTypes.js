import React, { Component } from 'react'

import NodeTypeAdd from './NodeTypeAdd'
import NodeTypeView from './NodeTypeView'
import EmptyPlaceholder from '../../Placeholders/EmptyPlaceholder'
import * as actionType from '../../Helpers/cg.types'
import cgu from '../../Helpers/cg.util'
import './NodeTypes.scss'

class NodeTypes extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: props.nodeTypes
    }
  }

  onButtonClick = action => {
    switch (action.type) {
      case actionType.ADD_ITEM:
        this.addNewNodeType(action)
        break
      case actionType.DELETE_ITEM:
        this.deleteNodeType(action)
        break
      default:
        console.warn(
          'NodeTypes.onButtonClick...NOT SUPPORTED...',
          action
        )
    }
  }
  addNewNodeType(action) {
    let { items } = this.state
    let { color, label } = action.payload
    const id = cgu.constructPropertyId(label)
    let newType = {
      id,
      color: `#${color}`,
      label
    }
    items[id] = newType
    //update state
    this.setState({
      items
    })
  }
  saveNodeType(action) {
    // debugger
    let { items } = this.state
    let { id, label, color } = action.payload

    items[id] = {
      id,
      label,
      color
    }
    //update state
    this.setState({
      items
    })
  }
  deleteNodeType(action) {
    // debugger
    let { items } = this.state
    let { id } = action.payload
    delete items[id]
    //update state
    this.setState({
      items
    })
  }
  getNodeTypeView = () => {
    // debugger
    const { items } = this.state
    const keys = Object.keys(items)
    if (keys.length === 0) {
      return (
        <EmptyPlaceholder
          title="Node types not defined"
          message="Please define at least one node type"
        />
      )
    }
    let innerHtml = (
      <NodeTypeView
        nodeTypes={items}
        onButtonClick={this.onButtonClick}
      />
    )
    return innerHtml
  }
  getNodeTypeAdd = () => {
    return <NodeTypeAdd onButtonClick={this.onButtonClick} />
  }
  render() {
    return (
      <section className="NodeTypes">
        <div className="NodeTypes-add">
          <p className="NodeTypes-text">
            Define color as hex value (without # sign) and the
            label of new node type. Click on plus sign (+) on
            the right to add new node type. Repeat the process
            for each node type you want to add.{' '}
            <strong>
              You need to add at least one node type.
            </strong>
            <br />
            <br />
            If you need assistance with defining node colors
            &nbsp;
            <a
              href="https://coolors.co/browser/latest/1"
              target="_new">
              check this website for the suggestions
            </a>
            .
          </p>
          <h4 className="NodeTypes-title">Add node type</h4>
          {this.getNodeTypeAdd()}
        </div>
        <div className="NodeTypes-list">
          <h4 className="NodeTypes-title">Defined types</h4>
          {this.getNodeTypeView()}
        </div>
      </section>
    )
  }
  componentWillUnmount() {
    this.props.setNodeTypes(this.state.items)
  }
}

export default NodeTypes
