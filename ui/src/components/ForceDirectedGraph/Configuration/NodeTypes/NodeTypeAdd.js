import React, { Component } from 'react'

import { InputText } from '../../Inputs'
import IconButton from '../../Toolbox/IconButton'
import { addNodeTypeButtons } from './toolboxButtons'
import cgu from '../../Helpers/cg.util'
import * as actionType from '../../Helpers/cg.types'

import './NodeTypeAdd.scss'

class NodeTypeAdd extends Component {
  state = {
    color: '',
    label: '',
    valid: false
  }
  required = ['id', 'color', 'label']
  handleInputChange = event => {
    const { name, value } = event.target
    this.setState({
      [name]: value,
      valid: cgu.isConfigValid(
        this.state,
        this.required,
        event.target
      )
    })
  }
  initStateFromProps = () => {
    if (this.state === null) {
      setTimeout(() => {
        this.setState({
          ...this.props,
          valid: false
        })
      }, 1)
    }
  }
  onButtonClick = action => {
    action.payload = {
      ...this.state
    }
    this.props.onButtonClick(action)
    this.setState({
      color: '',
      label: '',
      valid: false
    })
  }
  getNavButtons = () => {
    let navHtml = addNodeTypeButtons.map(btn => {
      //add onButtonClick method to icon
      btn['onIconBtnClick'] = this.onButtonClick.bind(
        this,
        btn.action
      )
      if (
        btn.action.type === actionType.ADD_ITEM &&
        this.state !== null
      ) {
        //debugger
        const { valid } = this.state
        btn.disabled = !valid
      }
      return <IconButton key={btn.action.type} {...btn} />
    })
    return navHtml
  }
  render() {
    // debugger
    let labelHtml, colorHtml
    const { label, color } = this.state
    let hexColor
    if (color !== '') {
      hexColor = `#${color}`
    } else {
      hexColor = `#fff`
    }
    colorHtml = (
      <div className="NodeType-color">
        <div
          className="NodeType-color-sample"
          style={{ backgroundColor: hexColor }}
          title="Color sample"
        />
        <span className="NodeType-color-hex">#</span>
        <InputText
          setFocus={true}
          name="color"
          placeholder="Node color"
          hasContent={color !== ''}
          maxlength="6"
          onChange={this.handleInputChange}
          value={color}
          inputInfo="Provide hex # value."
        />
      </div>
    )
    labelHtml = (
      <InputText
        name="label"
        placeholder="Label"
        hasContent={label !== ''}
        maxlength="50"
        onChange={this.handleInputChange}
        value={label}
        inputInfo="Max. length of 50 chars."
      />
    )

    return (
      <div className="NodeType-item">
        {colorHtml}
        {labelHtml}
        <div className="NodeType-nav">
          {this.getNavButtons()}
        </div>
      </div>
    )
  }
}

export default NodeTypeAdd
