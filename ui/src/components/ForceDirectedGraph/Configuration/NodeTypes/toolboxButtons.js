import IconDelete from '../../../../styles/icons/baseline-delete-24px.svg'
import IconAdd from '../../../../styles/icons/baseline-add-24px.svg'

import * as actionType from '../../Helpers/cg.types'

const addNodeTypeButtons = [
  {
    action: {
      type: actionType.ADD_ITEM
    },
    label: 'Add new node type',
    icon: {
      src: IconAdd,
      alt: 'add'
    },
    disabled: false
  }
]

const viewNodeTypeButtons = [
  {
    action: {
      type: actionType.DELETE_ITEM
    },
    label: 'Delete',
    icon: {
      src: IconDelete,
      alt: 'delete'
    },
    disabled: false
  }
]

export { addNodeTypeButtons, viewNodeTypeButtons }
