import React, { Component } from 'react'

import './Configuration.scss'

import Toolbox from '../Toolbox/Toolbox'
import { toolboxButtonsSettings } from './toolboxButtons'
import * as actionType from '../Helpers/cg.types'
import StatusBar from './StatusBar/StatusBar'

//steps
import LinksMatrix from './LinksMatrix/LinksMatrix'
import NodeTypes from './NodeTypes/NodeTypes'
import NodeProps from './NodeProps/NodeProps'
// info pages
import IntroStep from './InfoScreens/IntroStep'
import SummaryStep from './InfoScreens/SaveConfiguration'
// defaults
import {
  defaultNodeProps,
  defaultSimCfg
} from '../Helpers/cg.config'

class Configuration extends Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 0,
      newSetup: props.newSetup,
      maxStep: 4,
      minStep: 0,
      nodeTypes: this.initConfig(props, 'nodeTypes'),
      nodeProps: this.initConfig(props, 'nodeProps'),
      linkRules: this.initConfig(props, 'linkRules'),
      simulation: this.initConfig(props, 'simulation')
    }
    if (props.newSetup === false) {
      //we skip intro step if not new setup
      this.state.step = 1
    }
  }
  titles = [
    'Intro',
    '1. Define node types',
    '2. Define custom properties',
    '3. Define linking rules',
    'Save configuration'
  ]
  initConfig(props, section) {
    const { config } = props
    if (
      typeof config === 'undefined' ||
      config.hasOwnProperty(section) === false
    ) {
      if (section === 'nodeProps') {
        return {
          defaultProps: defaultNodeProps,
          typeProps: {},
          typeSize: {}
        }
      } else if (section === 'simulation') {
        return defaultSimCfg
      } else {
        return {}
      }
    } else {
      return config[section]
    }
  }

  onToolboxButton = action => {
    let next = this.state.step
    // console.log('action...', action)
    switch (action.type) {
      case actionType.CLOSE_MODAL:
        this.props.onModalAction(action)
        break
      case actionType.NEXT_STEP:
        next += 1
        if (this.state.maxStep >= next) {
          this.setState({
            step: next
          })
        }
        break
      case actionType.PREV_STEP:
        next -= 1
        if (this.state.minStep <= next) {
          this.setState({
            step: next
          })
        }
        break
      case actionType.SAVE_CONFIGURATION:
        this.onSaveConfiguration()
        break
      default:
        console.warn(
          `Configuration.onToolboxButton...action  [${
            action.type
          }] not supported`
        )
    }
  }

  getToolboxButtons = () => {
    const {
      step,
      maxStep,
      minStep,
      nodeTypes,
      newSetup
    } = this.state
    const buttons = [...toolboxButtonsSettings]
    if (step === minStep) {
      buttons[0].disabled = true
      buttons[1].disabled = false
      buttons[2].disabled = true
    } else if (step === maxStep) {
      //last step is summary
      buttons[0].disabled = false
      buttons[1].disabled = true
      buttons[2].disabled = false
    } else if (step === 3) {
      //enable back button
      buttons[0].disabled = false
      //disable save button
      buttons[2].disabled = true
      if (Object.keys(nodeTypes).length === 0) {
        //if reached step 3 but did not
        //defined nodeTypes
        //disable next button
        buttons[1].disabled = true
      } else {
        //enable next button
        buttons[1].disabled = false
      }
    } else {
      buttons[0].disabled = false
      buttons[1].disabled = false
      buttons[2].disabled = true
    }
    if (newSetup === true) {
      //not allowed to close if first setup
      buttons[3].disabled = true
    } else {
      buttons[3].disabled = false
    }
    return buttons
  }

  loadStepComponent = () => {
    const { step, nodeTypes, nodeProps, linkRules } = this.state
    switch (step) {
      case 0:
        return <IntroStep />
      case 1:
        return (
          <NodeTypes
            nodeTypes={nodeTypes}
            setNodeTypes={this.onSetNodeTypes}
          />
        )
      case 2:
        return (
          <NodeProps
            nodeTypes={nodeTypes}
            nodeProps={nodeProps}
            setNodeProps={this.onSetNodeProps}
          />
        )
      case 3:
        return (
          <LinksMatrix
            nodeTypes={nodeTypes}
            linkRules={linkRules}
            setLinksMatrix={this.onSetLinksMatrix}
          />
        )
      case 4:
        return (
          <SummaryStep
            data={this.state}
            onSave={this.onSaveConfiguration}
          />
        )
      default:
        return <h3>Unknown step id...</h3>
    }
  }
  onSetNodeTypes = nodeTypes => {
    //console.log('onSetNodeTypes...', nodeTypes)
    this.setState({
      nodeTypes
    })
  }

  onSetNodeProps = nodeProps => {
    //console.log('onSetNodeProps...', nodeProps)
    this.setState({
      nodeProps
    })
  }

  onSetLinksMatrix = linkRules => {
    //console.log('onSetLinksMatrix...', linkRules)
    this.setState({
      linkRules
    })
  }

  onSaveConfiguration = () => {
    //console.log('Save configuration....', this.state)
    const {
      nodeTypes,
      nodeProps,
      linkRules,
      simulation
    } = this.state
    const { onModalAction } = this.props
    const newConfig = {
      nodeTypes,
      nodeProps,
      linkRules,
      simulation
    }
    //pass save action
    onModalAction({
      type: actionType.SAVE_CONFIGURATION,
      payload: newConfig
    })
  }

  render() {
    const { step, maxStep } = this.state
    return (
      <section className="Configuration-frame">
        <Toolbox
          title="Configuration Wizard"
          buttons={this.getToolboxButtons()}
          onButtonClick={this.onToolboxButton}
        />
        <div className="Configuration-body">
          <header className="Configuration-status-header">
            <h1>{this.titles[step]}</h1>
            <StatusBar pos={step} max={maxStep - 1} />
          </header>
          <article className="Configuration-body-main">
            {this.loadStepComponent()}
          </article>
        </div>
      </section>
    )
  }
}

export default Configuration
