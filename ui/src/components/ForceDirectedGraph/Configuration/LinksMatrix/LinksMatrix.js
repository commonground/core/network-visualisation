import React, { Component } from 'react'

import EmptyPlaceholder from '../../Placeholders/EmptyPlaceholder'
import './LinksMatrix.scss'

class LinksMatrix extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nodeTypes: props.nodeTypes,
      linkRules: this.initLinkRulesMatrix(
        props.nodeTypes,
        props.linkRules
      )
    }
  }
  initLinkRulesMatrix(nodeTypes, linkRules) {
    const types = Object.keys(nodeTypes)
    const links = Object.keys(linkRules)
    //check if linkRules has other keys
    //than nodeTypes. In that case we
    //remove these keys
    links.forEach(key => {
      if (types.indexOf(key) === -1) {
        //remove from nodeTypes
        delete linkRules[key]
      }
    })
    types.forEach(type => {
      if (linkRules.hasOwnProperty(type) === false) {
        linkRules[type] = {}
      }
      types.forEach(col => {
        if (linkRules[type].hasOwnProperty(col) === false) {
          //set default value to true
          linkRules[type][col] = true
        }
      })
    })
    return linkRules
  }
  getTableHeadHtml = () => {
    const { linkRules, nodeTypes } = this.state
    const keys = Object.keys(linkRules)
    return (
      <tr className="LinksMatrix-th">
        <td />
        {keys.map((type, i) => {
          let item = nodeTypes[type]
          return (
            <th className="LinksMatrix-type" key={`type_${i}`}>
              {item.label}
            </th>
          )
        })}
      </tr>
    )
  }
  getTableBody = () => {
    const { linkRules, nodeTypes } = this.state
    const keys = Object.keys(linkRules)
    let bodyHtml = keys.map((rowType, row) => {
      let item = nodeTypes[rowType]
      return (
        <tr key={`type_${row}`} className="LinksMatrix-tr">
          <td className="LinksMatrix-type">{item.label}</td>
          {keys.map((colType, col) => {
            let value = linkRules[rowType][colType]
            return (
              <td
                key={`cell_${row}_${col}`}
                className={value ? 'active' : 'not-active'}
                onClick={this.onToggleMatrixCell.bind(this, {
                  cell: [row, col],
                  types: [rowType, colType]
                })}
              />
            )
          })}
        </tr>
      )
    })
    return bodyHtml
  }
  getMatrixContent() {
    const { linkRules } = this.state
    const keys = Object.keys(linkRules)
    let matrixHtml
    // debugger
    if (keys.length === 0) {
      matrixHtml = (
        <EmptyPlaceholder
          title="Node types not defined"
          message="Go back to step 1 and define at least one node type"
        />
      )
    } else {
      matrixHtml = (
        <table className="LinksMatrix-tbl">
          <thead>{this.getTableHeadHtml()}</thead>
          <tbody>{this.getTableBody()}</tbody>
        </table>
      )
    }
    return matrixHtml
  }
  onToggleMatrixCell = action => {
    const { linkRules } = this.state
    const { types } = action
    //toggle value
    linkRules[types[0]][types[1]] = !linkRules[types[0]][
      types[1]
    ]
    //check if mirroring is required
    if (types[0] !== types[1]) {
      linkRules[types[1]][types[0]] = !linkRules[types[1]][
        types[0]
      ]
    }
    //update state
    this.setState({
      linkRules
    })
  }
  checkForNoConnectionsAllowed = () => {
    const { linkRules } = this.state
    const keys = Object.keys(linkRules)
    let noLinks = true

    keys.forEach(key => {
      let rule = linkRules[key]
      let keys2 = Object.keys(rule)
      keys2.forEach(col => {
        let cell = rule[col]
        if (cell === true) {
          noLinks = false
        }
      })
    })

    if (noLinks) {
      return (
        <p className="NodeTypes-text-bold">
          Note! If you remove all check marks the nodes will not
          be able to connect to each other. Enable at least
          connection between the nodes of same type by placing
          check marks along the diagonal.
        </p>
      )
    } else {
      return null
    }
  }
  render() {
    return (
      <section className="LinksMatrix">
        <p className="NodeTypes-text">
          Define which node type is allowed to connect to
          another type by clicking on the cell in the matrix
          bellow. The checkmark will appear for the node types
          which are allowed to be linked. Default setting allows
          connection between all node types. Note that matrix is
          &quot;symmetric&quot; and clicking on one cell will
          also add checkmark at the node in the
          &quot;mirrored&quot; cell.
        </p>
        <h4 className="NodeTypes-title">
          Allowed connections (matrix)
        </h4>
        {this.getMatrixContent()}
        {this.checkForNoConnectionsAllowed()}
      </section>
    )
  }
  componentWillUnmount() {
    this.props.setLinksMatrix(this.state.linkRules)
  }
}

export default LinksMatrix
