import React, { Component } from 'react'

import './ToggleButton.scss'

class ToggleButton extends Component {
  render() {
    //debugger
    let { on, onToggleClick } = this.props
    let innerHtml = ' '
    if (on) {
      innerHtml = 'Y'
    }
    return (
      <button
        className="ToggleButton-button"
        onClick={onToggleClick}>
        {innerHtml}
      </button>
    )
  }
}

export default ToggleButton
