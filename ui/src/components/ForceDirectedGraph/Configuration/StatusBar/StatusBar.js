import React from 'react'

import './StatusBar.scss'
const StatusBar = ({ pos = 1, max = 3 }) => {
  let innerHtml = []
  for (let i = 1; i <= max; i++) {
    if (i <= pos) {
      innerHtml.push(
        <div
          key={`point-${i}`}
          className="StatusBar-point completed"
        />
      )
    } else {
      innerHtml.push(
        <div key={`point-${i}`} className="StatusBar-point" />
      )
    }
  }
  return <section className="StatusBar">{innerHtml}</section>
}

export default StatusBar
