import React, { Component } from 'react'

import IconButton from '../../Toolbox/IconButton'
import { viewNodeTypeButtons } from './toolboxButtons'

import './NodePropsView.scss'

class NodePropsView extends Component {
  state = null

  onButtonClick = action => {
    this.props.onButtonClick(action)
  }
  getNavButtons = id => {
    let navHtml = viewNodeTypeButtons.map(btn => {
      //new object
      let newBtn = {
        ...btn,
        action: {
          type: btn.action.type,
          payload: {
            id
          }
        },
        onIconBtnClick: this.onButtonClick
      }
      return <IconButton key={btn.action.type} {...newBtn} />
    })
    return navHtml
  }
  getTableHeader() {
    return (
      <tr>
        <th>Label</th>
        <th>Type</th>
        <th>Min</th>
        <th>Max</th>
        <th>Step</th>
        <th>Required</th>
        <th />
      </tr>
    )
  }
  getPropRow(nodeProp, defaultProp = false) {
    const {
      id,
      label,
      type,
      min,
      max,
      step,
      required
    } = nodeProp
    let innerHtml = (
      <tr key={id} className="NodePropsView-row">
        <td>{label}</td>
        <td>{type}</td>
        <td>{min}</td>
        <td>{max}</td>
        <td>{step ? step : 'n/a'}</td>
        <td>{required ? 'Yes' : 'No'}</td>
        <td className="NodePropsView-nav">
          {// if not default prop AND onButtonClick function provided
          defaultProp === false && this.props.onButtonClick
            ? this.getNavButtons(id)
            : null}
        </td>
      </tr>
    )
    return innerHtml
  }

  getTableBody() {
    const { nodeProps, defaultProps } = this.props
    const keys = Object.keys(nodeProps)
    const defs = Object.keys(defaultProps)
    let tblBodyHtml = []
    //tblBodyHtml.push()
    defs.forEach(key => {
      let defProp = defaultProps[key]
      //show only defaultProps which are
      //not internal system props and
      //are required to be provided by user input
      if (defProp.system === false) {
        //send defaultProp flag
        let tblRow = this.getPropRow(defProp, true)
        tblBodyHtml.push(tblRow)
      }
    })
    keys.forEach(key => {
      let nodeProp = nodeProps[key]
      let tblRow = this.getPropRow(nodeProp, false)
      tblBodyHtml.push(tblRow)
    })
    return tblBodyHtml
  }

  render() {
    return (
      <table className="NodePropsView">
        <thead>{this.getTableHeader()}</thead>
        <tbody>{this.getTableBody()}</tbody>
      </table>
    )
  }
}

export default NodePropsView
