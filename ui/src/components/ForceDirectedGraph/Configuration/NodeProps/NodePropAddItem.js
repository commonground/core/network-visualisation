import React, { Component } from 'react'

import {
  InputText,
  InputNumber,
  InputSelect,
  InputCustomCheckbox
} from '../../Inputs'
import IconButton from '../../Toolbox/IconButton'
import { addNodeTypeButtons } from './toolboxButtons'
import cgu from '../../Helpers/cg.util'
import * as actionType from '../../Helpers/cg.types'

import './NodePropAddItem.scss'

class NodePropAddItem extends Component {
  state = {
    label: '',
    type: actionType.TEXT,
    min: '5',
    max: '50',
    step: '1',
    required: false,
    nodeSize: false,
    valid: false
  }
  types = [
    {
      value: actionType.TEXT,
      label: 'Text'
    },
    {
      value: actionType.SLIDER,
      label: 'Slider'
    }
  ]
  resetState() {
    this.setState({
      label: '',
      type: actionType.TEXT,
      min: '5',
      max: '50',
      step: '1',
      required: false,
      nodeSize: false,
      valid: false
    })
  }
  required = ['label', 'type', 'min', 'max', 'required']
  handleInputChange = event => {
    const { name, value } = event.target
    this.setState({
      [name]: value,
      valid: cgu.isConfigValid(
        this.state,
        this.required,
        event.target
      )
    })
  }
  initStateFromProps = () => {
    if (this.state === null) {
      setTimeout(() => {
        this.setState({
          ...this.props,
          valid: false
        })
      }, 1)
    }
  }
  prepData() {
    const {
      label,
      type,
      min,
      max,
      required,
      step,
      nodeSize
    } = this.state

    let data = {
      id: cgu.constructPropertyId(label),
      label,
      type,
      min,
      max,
      required
    }
    if (type === this.types[1].value) {
      data['step'] = step || 1
      data['nodeSize'] = nodeSize
    }
    return data
  }

  onButtonClick = action => {
    action.payload = this.prepData()
    this.props.onButtonClick(action)
    this.resetState()
  }
  getNavButtons = () => {
    let navHtml = addNodeTypeButtons.map(btn => {
      //add onButtonClick method to icon
      btn['onIconBtnClick'] = this.onButtonClick.bind(
        this,
        btn.action
      )
      if (
        btn.action.type === actionType.ADD_ITEM &&
        this.state !== null
      ) {
        //debugger
        const { valid } = this.state
        btn.disabled = !valid
      }
      return <IconButton key={btn.action.type} {...btn} />
    })
    return navHtml
  }
  render() {
    const {
      label,
      type,
      min,
      max,
      step,
      required,
      nodeSize
    } = this.state

    const selectHtml = (
      <InputSelect
        name="type"
        label="Type"
        options={this.types}
        onChange={this.handleInputChange}
        value={type}
      />
    )
    const labelHtml = (
      <InputText
        setFocus={true}
        name="label"
        placeholder="Label"
        hasContent={true}
        maxlength="20"
        onChange={this.handleInputChange}
        value={label}
        inputInfo=""
      />
    )
    const minHtml = (
      <InputNumber
        key="min"
        name="min"
        placeholder="Min"
        hasContent={true}
        onChange={this.handleInputChange}
        value={min}
        inputInfo=""
      />
    )
    const maxHtml = (
      <InputNumber
        key="max"
        name="max"
        placeholder="Max"
        hasContent={true}
        onChange={this.handleInputChange}
        value={max}
        inputInfo=""
      />
    )
    const stepHtml = (
      <InputNumber
        key="step"
        name="step"
        placeholder="Step"
        hasContent={true}
        min="1"
        onChange={this.handleInputChange}
        value={step}
        inputInfo=""
      />
    )

    const requiredHtml = (
      <InputCustomCheckbox
        key="required"
        name="required"
        placeholder="Required"
        onChange={this.handleInputChange}
        value={required}
        inputInfo=""
      />
    )

    const nodeSizeHtml = (
      <InputCustomCheckbox
        key="nodeSize"
        name="nodeSize"
        placeholder="Node size"
        onChange={this.handleInputChange}
        value={nodeSize}
        inputInfo=""
      />
    )

    return (
      <section className="NodePropAddItem">
        <div className="NodePropAddItem-input">
          {labelHtml}
          {selectHtml}
          {minHtml}
          {maxHtml}
          {type === actionType.SLIDER ? stepHtml : null}
          {type === actionType.SLIDER ? nodeSizeHtml : null}
          {requiredHtml}
        </div>
        <div className="NodePropAddItem-nav">
          {this.getNavButtons()}
        </div>
      </section>
    )
  }
}

export default NodePropAddItem
