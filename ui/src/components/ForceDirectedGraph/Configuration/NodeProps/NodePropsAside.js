import React from 'react'

import './NodePropsAside.scss'

const NodePropsAside = props => {
  let { nodeTypes, nodeTypeKey, setNodeType } = props
  let keys = Object.keys(nodeTypes)
  let innerHtml = (
    <ul>
      {keys.map(key => {
        let item = nodeTypes[key]
        let classes = `NodePropsAside-nav noselect ${
          nodeTypeKey === key ? 'active' : ''
        }`
        return (
          <li
            key={item.id}
            className={classes}
            onClick={setNodeType.bind(this, item.id)}>
            {item.label}
          </li>
        )
      })}
    </ul>
  )
  return (
    <aside key="node-props-aside" className="NodePropsAside">
      {innerHtml}
    </aside>
  )
}

export default NodePropsAside
