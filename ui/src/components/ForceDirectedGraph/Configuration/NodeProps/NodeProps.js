import React, { Component } from 'react'

import NodePropsAside from './NodePropsAside'
import NodePropAddItem from './NodePropAddItem'
import NodePropsView from './NodePropsView'
import EmptyPlaceholder from '../../Placeholders/EmptyPlaceholder'
import * as actionType from '../../Helpers/cg.types'
import {
  constantNodeSize,
  getDefaultNodeSize
} from '../../Helpers/cg.config'
import './NodeProps.scss'

class NodeProps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nodeTypes: props.nodeTypes,
      nodeProps: this.initNodeProps(
        props.nodeTypes,
        props.nodeProps,
        props.typeSize
      ),
      nodeTypeKey: this.getFirstNodeTypeKey(props.nodeTypes),
      editTypeProps: []
    }
  }

  initNodeProps(nodeTypes = {}, nodeProps = {}) {
    let {
      defaultProps,
      typeProps = {},
      typeSize = {}
    } = nodeProps
    let keys = Object.keys(nodeTypes)
    keys.forEach(key => {
      if (typeProps.hasOwnProperty(key) === false) {
        //add object for this type
        typeProps[key] = {}
      }
      if (typeSize.hasOwnProperty(key) === false) {
        //add default node size for this type
        typeSize[key] = this.getDefaultNodeSizeProps()
      } else {
        const nodeSize = typeSize[key]
        if (nodeSize.hasOwnProperty('type') === false) {
          //if type not defined we set defaults
          typeSize[key] = this.getDefaultNodeSizeProps()
        } else if (
          typeSize[key].type === actionType.CONSTANT_NODE_SIZE
        ) {
          //update defaultNode size because
          //it can be chaned by user in sim settings (SimSettings.js)
          typeSize[key].value = getDefaultNodeSize()
        }
      }
    })
    return {
      defaultProps,
      typeProps,
      typeSize
    }
  }
  getDefaultNodeSizeProps() {
    return {
      ...constantNodeSize,
      value: getDefaultNodeSize()
    }
  }
  getFirstNodeTypeKey(nodeTypes) {
    let keys = Object.keys(nodeTypes)
    return keys[0]
  }

  setNodeType = nodeTypeKey => {
    this.setState({
      nodeTypeKey
    })
  }
  getCustomPropsHtml = () => {
    const {
      nodeProps: { typeProps },
      nodeProps: { defaultProps },
      nodeTypeKey
    } = this.state

    const customProps = typeProps[nodeTypeKey]

    let innerHtml = (
      <NodePropsView
        defaultProps={defaultProps}
        nodeProps={customProps}
        onButtonClick={this.onButtonClick}
      />
    )
    return innerHtml
  }
  getNodeSizeType = nodeSize => {
    if (nodeSize.type === actionType.CONSTANT_NODE_SIZE) {
      return (
        <span className="NodeProps-nodesize">
          Node size will be constant. The circle radius will be
          set to {nodeSize.value}px.
        </span>
      )
    } else {
      return (
        <span className="NodeProps-nodesize">
          Node size will be calculated using the values of&nbsp;
          <strong>{nodeSize.label}</strong>.
        </span>
      )
    }
  }
  getNodePropsHtml() {
    const { nodeTypes, nodeProps, nodeTypeKey } = this.state
    const selectedType = nodeTypes[nodeTypeKey]
    const selectedSize = nodeProps.typeSize[nodeTypeKey]

    let selectedTypeLabel = 'empty'
    let nodePropsHtml = []

    if (typeof selectedType === 'undefined') {
      return (
        <EmptyPlaceholder
          title="Node types not defined"
          message="Please go back to previous step and define at least one node type."
        />
      )
    } else {
      selectedTypeLabel = selectedType.label

      nodePropsHtml.push(
        <NodePropsAside
          key="node-props-aside"
          nodeTypes={nodeTypes}
          nodeTypeKey={nodeTypeKey}
          setNodeType={this.setNodeType}
        />
      )

      nodePropsHtml.push(
        <div
          key="node-props-divider"
          className="NodeProps-divider"
        />
      )

      nodePropsHtml.push(
        <section
          key="node-props-body"
          className="NodeProps-body">
          <div className="NodeProps-edit">
            <div className="NodeProps-add">
              <h4 className="NodeTypes-title">
                Add custom property to {selectedTypeLabel}
              </h4>
              <NodePropAddItem
                onButtonClick={this.onButtonClick}
              />
            </div>
            <div className="NodeProps-list">
              <h4 className="NodeTypes-title">
                Defined properties
              </h4>
              {this.getCustomPropsHtml()}
            </div>
            <div className="NodeProps-list">
              <h4 className="NodeTypes-title">
                Node size calculation
              </h4>
              <p className="NodeTypes-text">
                The size of each {selectedTypeLabel} node can be
                constant or it can be calculated using the value
                of one of the custom properties. To use custom
                property for the size of {selectedTypeLabel}
                &nbsp; node set property &quot;Type&quot; to
                &quot;Slider&quot; and change &quot;Node
                size&quot; value to Yes.
                <br />
                <br />
                {this.getNodeSizeType(selectedSize)}
              </p>
            </div>
          </div>
        </section>
      )
      return nodePropsHtml
    }
  }
  onButtonClick = action => {
    // console.log('NodeProps.onButtonClick...', action)
    switch (action.type) {
      case actionType.ADD_ITEM:
        this.addCustomProp(action)
        break
      case actionType.DELETE_ITEM:
        this.deleteCustomProp(action)
        break
      default:
        console.warn(
          'NodeProps.onButtonClick...NOT SUPPORTED...',
          action
        )
    }
  }
  addCustomProp = action => {
    //console.log('addCustomProp...', action)
    const { id } = action.payload
    const { nodeProps, nodeTypeKey } = this.state
    const customProps = nodeProps.typeProps[nodeTypeKey]
    customProps[id] = action.payload
    this.updateCustomProps(customProps)
  }
  deleteCustomProp = action => {
    //console.log('deleteCustomProp...', action)
    const { id } = action.payload
    const { nodeProps, nodeTypeKey } = this.state
    const customProps = nodeProps.typeProps[nodeTypeKey]
    let useDefaultSize = false
    // check if this prop is used for node size calcluation
    if (customProps[id].nodeSize === true) {
      //use default size instead
      useDefaultSize = true
    }
    //remove prop
    delete customProps[id]
    this.updateCustomProps(customProps, useDefaultSize)
  }
  updateCustomProps(customProps, useDefaultSize = false) {
    const { nodeProps, nodeTypeKey } = this.state
    //new object
    const newNodeProps = {
      ...nodeProps,
      typeProps: {
        ...nodeProps.typeProps,
        [nodeTypeKey]: {
          ...customProps
        }
      }
    }
    if (useDefaultSize === true) {
      newNodeProps.typeSize[
        nodeTypeKey
      ] = this.getDefaultNodeSizeProps()
    } else {
      // check for nodeSize calculation
      let keys = Object.keys(customProps)
      keys.forEach(key => {
        let item = customProps[key]
        if (item.nodeSize) {
          newNodeProps.typeSize[nodeTypeKey] = {
            type: actionType.CUSTOM_NODE_SIZE,
            label: item.label,
            value: item.id
          }
        }
      })
    }
    //update state
    this.setState({
      nodeProps: newNodeProps
    })
  }
  render() {
    //debugger
    return (
      <React.Fragment>
        <p className="NodeTypes-text">
          Here you can define custom properties for each node
          type. On the left side of the page you see navigation
          panel with the node types you defined at previous
          step. At the right side you can define new custom
          property and add it using&nbsp;+&nbsp;button. You
          can&nbsp;
          <strong>
            define one custom property to be used to calculate
            the node size
          </strong>
          . See paragraph &quot;Node size calculation&quot;. For
          each node type the application creates
          &quot;Label&quot; property according to system
          requirements. Label property cannot be changed.
        </p>
        <section className="NodeProps">
          {this.getNodePropsHtml()}
        </section>
      </React.Fragment>
    )
  }
  componentWillUnmount() {
    this.props.setNodeProps(this.state.nodeProps)
  }
}

export default NodeProps
