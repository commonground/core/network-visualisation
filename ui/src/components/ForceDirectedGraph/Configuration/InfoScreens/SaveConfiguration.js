import React from 'react'

import './SaveConfiguration.scss'

import EmptyPlaceholder from '../../Placeholders/EmptyPlaceholder'

const SaveConfiguration = props => {
  return (
    <div className="SaveConfiguration">
      <EmptyPlaceholder
        title="You have completed all configuration steps!"
        message="You can go back and double check all definitions if you
        want. Otherwise click on Save button at the top right
        to save configuration."
      />
    </div>
  )
}

export default SaveConfiguration
