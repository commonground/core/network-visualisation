import React from 'react'

const IntroStep = () => {
  return (
    <div>
      <p>
        Before you can enjoy this magnificent application&nbsp;
        <b>three things need to be defined</b>. This wizard will
        guide you through necessary steps. Use
        &quot;previous&quot; and &quot;next&quot; buttons on the
        top right to navigate through the steps.
      </p>
      <h4 className="NodeTypes-title">1. Define node types</h4>
      <p>
        Each node in the visualization is represented as a
        circle. To visually distinct between different node
        groups (circles) you should define different node types.
        Each node type will have distinctive color you define at
        step 1 and custom properties you define at step 2.
      </p>
      <h4 className="NodeTypes-title">
        &nbsp;2. Define custom properties for each node type
      </h4>
      <p>
        You can define custom properties for each node type. All
        nodes of specific type should share the same properties.
        Note that one of the custom properties can be used to
        represent the size of node in the visualization. At the
        step 2 you will be able to select which custom property
        is used to calculate the size of the node (circle).
      </p>
      <h4 className="NodeTypes-title">
        &nbsp;3. Define linking rules
      </h4>
      <p>
        At this step you define which nodes can be linked. In
        the visualization, after adding specific node that will
        have one of the types defined at step 1, you can select
        to link that node to another node. During the linking
        process only nodes that are defined as allowed to be
        linked will be highlighted.
      </p>
    </div>
  )
}

export default IntroStep
