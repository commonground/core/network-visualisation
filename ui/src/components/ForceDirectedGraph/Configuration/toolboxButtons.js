import IconNext from '../../../styles/icons/baseline-navigate_next-24px.svg'
import IconPrevious from '../../../styles/icons/baseline-navigate_before-24px.svg'
import IconClose from '../../../styles/icons/baseline-clear-24px.svg'
import IconSave from '../../../styles/icons/baseline-save-24px.svg'

import * as actionType from '../Helpers/cg.types'

const toolboxButtonsSettings = [
  {
    action: {
      type: actionType.PREV_STEP
    },
    label: 'Previous step',
    icon: {
      src: IconPrevious,
      alt: 'previous'
    },
    disabled: false
  },
  {
    action: {
      type: actionType.NEXT_STEP
    },
    label: 'Next step',
    icon: {
      src: IconNext,
      alt: 'next'
    },
    disabled: false
  },
  {
    action: {
      type: actionType.SAVE_CONFIGURATION
    },
    label: 'Save',
    icon: {
      src: IconSave,
      alt: 'save'
    },
    disabled: true
  },
  {
    action: {
      type: actionType.CLOSE_MODAL
    },
    label: 'Close',
    icon: {
      src: IconClose,
      alt: 'close'
    },
    disabled: false
  }
]

export { toolboxButtonsSettings }
