import React from 'react'

import './LoaderBallTriangle.scss'

const LoaderBallTriangle = () => {
  return (
    <div className="ball-triangle-path">
      <div />
      <div />
      <div />
    </div>
  )
}

export default LoaderBallTriangle
