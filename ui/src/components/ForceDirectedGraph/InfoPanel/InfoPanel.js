import React, { Component } from 'react'

import './InfoPanel.scss'

class InfoPanel extends Component {
  infoPanel = React.createRef()
  togglePanel = () => {
    const { current: panel } = this.infoPanel
    panel.classList.toggle('show')
  }
  render() {
    return (
      <section className="InfoPanel" ref={this.infoPanel}>
        <button
          className="InfoPanel-btn-show IconButton"
          onClick={this.togglePanel}
          title="Show info panel">
          ?
        </button>
        <button
          className="InfoPanel-btn-hide btn"
          onClick={this.togglePanel}
          title="Close info panel">
          X
        </button>
        <article className="InfoPanel-body">
          {this.props.children}
        </article>
      </section>
    )
  }
}

export default InfoPanel
