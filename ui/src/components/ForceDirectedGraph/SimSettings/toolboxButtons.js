import IconRestoreDefault from '../../../styles/icons/baseline-settings_backup_restore-24px.svg'
import IconClose from '../../../styles/icons/baseline-clear-24px.svg'
import IconSave from '../../../styles/icons/baseline-save-24px.svg'

import * as actionType from '../Helpers/cg.types'

const toolboxButtonsSettings = [
  {
    action: {
      type: actionType.SAVE_CONFIGURATION
    },
    label: 'Save',
    icon: {
      src: IconSave,
      alt: 'save'
    },
    disabled: true
  },
  {
    action: {
      type: actionType.DEFAULT_SETTINGS
    },
    label: 'Restore default',
    icon: {
      src: IconRestoreDefault,
      alt: 'restore'
    },
    disabled: false
  },
  {
    action: {
      type: actionType.CLOSE_MODAL
    },
    label: 'Cancel',
    icon: {
      src: IconClose,
      alt: 'cancel'
    },
    disabled: false
  }
]

export { toolboxButtonsSettings }
