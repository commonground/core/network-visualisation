import React, { Component } from 'react'

import cgu from '../Helpers/cg.util'
import { defaultSimCfg } from '../Helpers/cg.config'
import Toolbox from '../Toolbox/Toolbox'
import { toolboxButtonsSettings } from './toolboxButtons'
import * as actionType from '../Helpers/cg.types'
import IconTune from '../../../styles/icons/baseline-tune-24px.svg'
import { InputText, InputSlider } from '../Inputs'

import './SimSettings.scss'

class SimSettings extends Component {
  constructor(props) {
    super(props)
    this.state = this.initState(props.action)
  }
  initState(action) {
    const { type, payload } = action
    if (!payload) {
      console.error('SimModal.initState...payload not provided')
      return {
        items: {}
      }
    }
    let state = {
      id: null,
      type: type,
      items: {},
      itemKeys: [],
      valid: false
    }
    // default/system props
    let keys = Object.keys(payload)
    keys.forEach(key => {
      let props = payload[key]
      if (props.system === false) {
        state.items[key] = {
          ...props
        }
        //check required
        if (props.required === true) {
          this.required.push(key)
        }
      }
    })
    //preserve item order
    //in safari item property order changes
    //at each state update
    state.itemKeys = Object.keys(state.items)
    return state
  }
  //list of required fields
  required = []
  /**
   * Combines original payload data
   * with new input from state
   */
  updatePayload = action => {
    const { items } = this.state
    const { payload: orgData } = action
    const keys = Object.keys(items)
    let payload = {
      ...orgData
    }
    keys.forEach(key => {
      let item = items[key]
      if (payload.hasOwnProperty(key) === false) {
        //add new variable
        payload[key] = item
      } else {
        //update existing
        if (item.type === actionType.SLIDER) {
          if (item.step >= 1) {
            payload[key]['value'] = parseInt(item.value)
          } else {
            payload[key]['value'] = parseFloat(item.value)
          }
        } else {
          payload[key]['value'] = item.value
        }
        if (payload[key]['info'] !== item.info) {
          //update var info
          payload[key]['info'] = item.info
        }
        if (payload[key]['system'] !== item.system) {
          //update system flag
          payload[key]['system'] = item.system
        }
      }
    })
    return payload
  }
  onSave = () => {
    //console.log('onSave...', this.state.items)
    const { action } = this.props
    let payload = this.updatePayload(action)
    this.props.onModalAction({
      type: this.props.action.type,
      payload
    })
  }
  onCancel = () => {
    this.props.onModalAction({
      type: actionType.CANCEL_ACTION
    })
  }
  handleInputChange = event => {
    const { name, value } = event.target
    const { items } = this.state
    const item = items[name]
    if (item) {
      let updatedItems = {
        ...items,
        [name]: {
          ...item,
          value
        }
      }
      let isValid = cgu.isModalValid(
        this.state.items,
        this.required,
        event.target
      )
      this.setState({
        items: updatedItems,
        valid: isValid
      })
    } else {
      console.error(
        `handleInputChange...item ${name} MISSSING!`
      )
    }
  }
  getHeaderProps = () => {
    return {
      title: 'Tune simulation settings',
      img: {
        src: IconTune,
        alt: 'tune sim'
      }
    }
  }
  innerHtml = () => {
    const { items, itemKeys } = this.state
    let html = []
    itemKeys.forEach((key, pos) => {
      let item = items[key]
      let inputInfo = ''
      if (item.required) {
        inputInfo += 'Required. '
      } else {
        inputInfo += 'Optional. '
      }

      if (item.type === actionType.SLIDER) {
        inputInfo += `Values between ${item.min} and ${
          item.max
        }, increment by ${item.step}. ${item.info}`

        html.push(
          <InputSlider
            key={key}
            setFocus={pos === 0}
            name={key}
            placeholder={item.label}
            hasContent={item.value !== ''}
            min={item.min}
            max={item.max}
            step={item.step}
            onChange={this.handleInputChange}
            value={item.value}
            inputInfo={inputInfo}
          />
        )
      } else {
        inputInfo += `Text between ${item.min} and ${
          item.max
        } chars. ${item.info}`
        html.push(
          <InputText
            key={key}
            setFocus={pos === 0}
            name={key}
            placeholder={item.label}
            hasContent={item.value !== ''}
            maxlength={item.max}
            onChange={this.handleInputChange}
            value={item.value}
            inputInfo={inputInfo}
          />
        )
      }
    })
    if (html.length === 0) {
      return (
        <div>
          <h1>Application error</h1>
          <p>
            Cannot provide required information. Please notify
            system administration about this error:&nbsp;
            <b>NodeModal.innerHtml missing!</b>
          </p>
        </div>
      )
    } else {
      return html
    }
  }
  /**
   * Load default settings from configuration file
   */
  loadDefaultSettings() {
    let defaults = this.initState({
      type: this.props.action.type,
      payload: defaultSimCfg
    })
    this.setState(defaults)
    this.setState({
      valid: true
    })
  }

  onToolboxButton = action => {
    switch (action.type) {
      case actionType.DEFAULT_SETTINGS:
        this.loadDefaultSettings()
        break
      case actionType.SAVE_CONFIGURATION:
        this.onSave()
        break
      default:
        //cancel
        this.onCancel()
    }
  }
  getToolboxButtons() {
    let btns = toolboxButtonsSettings
    btns[0].disabled = !this.state.valid
    return btns
  }
  render() {
    let sides = {}
    let items = this.innerHtml()
    if (items.length > 1) {
      let middle = Math.ceil(items.length / 2)
      sides = {
        left: items.slice(0, middle),
        right: items.slice(middle)
      }
    } else {
      sides = {
        left: items,
        rigth: null
      }
    }

    return (
      <article className="SimSettings">
        <section className="SimSettings-content">
          <Toolbox
            title="Simulation settings"
            buttons={this.getToolboxButtons()}
            onButtonClick={this.onToolboxButton}
          />
          <div className="SimSettings-body">
            <div className="SimSettings-body-col">
              {sides.left}
            </div>
            <div className="SimSettings-body-col">
              {sides.right}
            </div>
          </div>
        </section>
      </article>
    )
  }
}

export default SimSettings
