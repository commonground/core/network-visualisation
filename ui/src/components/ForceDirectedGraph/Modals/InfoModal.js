import React, { Component } from 'react'

import ModalWindow from './ModalWindow'
import ModalHeader from './ModalHeader'
import * as actionType from '../Helpers/cg.types'
import BaseLineWarning from '../../../styles/icons/baseline-warning-24px.svg'

import './InfoModal.scss'

export class InfoModal extends Component {
  onOK = () => {
    this.props.onModalAction({
      type: actionType.CLOSE_MODAL
    })
  }
  render() {
    const headerProps = {
      title: this.props.title,
      img: {
        src: BaseLineWarning,
        alt: 'warning'
      }
    }
    const { btnText = 'OK' } = this.props

    return (
      <ModalWindow>
        <ModalHeader {...headerProps} />
        <div className="Modal-body info-message">
          {this.props.message}
        </div>
        <div className="InfoModal-nav">
          <button
            className="btn btn-ok"
            type="button"
            onClick={this.onOK}>
            <span>{btnText}</span>
          </button>
        </div>
      </ModalWindow>
    )
  }
}

export default InfoModal
