import React from 'react'

import './ModalNav.scss'
/**
 * Modal navigation buttons,
 * Cancel and Save
 * @param {Object} props
 * @param {String} props.cancelText
 * @param {String} props.saveText
 * @param {Function} props.onCancel
 * @param {Function} props.onSave
 */
const ModalNav = props => {
  return (
    <div className="Modal-nav">
      <button
        className="btn cancel"
        type="button"
        onClick={props.onCancel}>
        <span>{props.cancelText}</span>
      </button>
      <button
        className="btn save"
        type="button"
        onClick={props.onSave}
        disabled={!props.canSave}>
        <span>{props.saveText}</span>
      </button>
    </div>
  )
}

export default ModalNav
