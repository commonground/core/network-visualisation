import React, { Component } from 'react'

import cgu from '../Helpers/cg.util'
import { decodeData } from '../Graphql/gqlHelpers'
import ModalWindow from './ModalWindow'
import ModalHeader from './ModalHeader'
import ModalNav from './ModalNav'
import * as actionType from '../Helpers/cg.types'
import BaseLineEdit from '../../../styles/icons/baseline-edit-24px.svg'
import { InputText, InputSlider } from '../Inputs'

class NodeModal extends Component {
  constructor(props) {
    super(props)
    //const { type, payload } = props.action
    this.state = this.initState(props.action)
  }
  initState(action) {
    const {
      type,
      payload: { nodeProps },
      payload: { nodeType },
      payload: { node }
    } = action

    if (!nodeType) {
      console.error(
        'NodeModal.initState...nodeType not provided'
      )
      return {
        items: {}
      }
    }
    if (!nodeProps) {
      console.error(
        'NodeModal.initState...nodeProps not provided'
      )
      return {
        items: {}
      }
    }
    const { defaultProps, typeProps } = nodeProps
    const { data: nodeData } = node
    let state = {
      id: null,
      type: nodeType.id,
      items: {},
      itemKeys: [],
      valid: false
    }
    // default/system props
    let keys = Object.keys(defaultProps)
    keys.forEach(key => {
      let props = defaultProps[key]
      //system values (id and type) are in
      //the root of state and not displayed in modal
      if (props.system === false) {
        state.items[key] = {
          ...props,
          src: 'defaultProps',
          value: ''
        }
        if (type === actionType.EDIT_NODE) {
          state.items[key].value = nodeData[key]
        }
        //default props are required
        this.required.push(key)
      }
    })
    // custom type props
    keys = Object.keys(typeProps)
    keys.forEach(key => {
      let props = typeProps[key]
      state.items[key] = {
        ...props,
        src: 'typeProps',
        value: ''
      }
      //check required
      if (props.required === true) {
        this.required.push(key)
      }
      if (
        props.type === actionType.SLIDER &&
        state.items[key].max
      ) {
        //set default slider value
        let delta = state.items[key].max - state.items[key].min
        let defVal = parseInt(state.items[key].min) + delta / 2
        state.items[key].value = Math.round(defVal)
      }
    })
    if (type === actionType.EDIT_NODE) {
      //get existing data from node
      let propsData = decodeData(nodeData.dataB64)
      //extract id
      state['id'] = nodeData['id']
      //extract other values from node data
      keys.forEach(key => {
        //let props = nodeProps[key]
        if (propsData.hasOwnProperty(key) === true) {
          state.items[key].value = propsData[key]
        } else {
          state.items[key].value = ''
        }
      })
    }
    //preserve item order
    //in safari item property order changes
    //at each state update
    state.itemKeys = Object.keys(state.items)
    return state
  }
  //list of required fields
  required = []

  getPayloadFromState() {
    const { items } = this.state
    const keys = Object.keys(items)
    let payload = {
      //system values
      id: this.state.id,
      type: this.state.type,
      //custom type properties
      data: {}
    }
    keys.forEach(key => {
      let item = items[key]
      if (item.src === 'defaultProps') {
        payload[key] = item.value
      } else {
        payload['data'][key] = item.value
      }
    })
    return payload
  }
  onSave = () => {
    //console.log('onSave...', this.state.items)
    let payload = this.getPayloadFromState()
    this.props.onModalAction({
      type: this.props.action.type,
      payload,
      sourceNode: this.props.action.payload.node
    })
  }
  onCancel = () => {
    this.props.onModalAction({
      type: actionType.CANCEL_ACTION
    })
  }
  handleInputChange = event => {
    const { name, value } = event.target
    const { items } = this.state
    const item = items[name]
    if (item) {
      let updatedItems = {
        ...items,
        [name]: {
          ...item,
          value
        }
      }
      let isValid = cgu.isModalValid(
        this.state.items,
        this.required,
        event.target
      )
      this.setState({
        items: updatedItems,
        valid: isValid
      })
    } else {
      console.error(
        `handleInputChange...item ${name} MISSSING!`
      )
    }
  }
  getHeaderProps = () => {
    const {
      type,
      payload: { nodeType }
    } = this.props.action
    let title = ''
    let img = {
      src: null,
      alt: null
    }
    if (nodeType) {
      if (type === actionType.EDIT_NODE) {
        title = `Edit ${nodeType.label.toLowerCase()}`
        img['src'] = BaseLineEdit
        img['alt'] = 'edit node'
      } else {
        title = `Add ${nodeType.label.toLowerCase()}`
        img['src'] = BaseLineEdit
        img['alt'] = 'add node'
      }
    }
    return {
      title,
      img
    }
  }
  innerHtml = () => {
    const { items, itemKeys } = this.state
    //const keys = Object.keys(items)
    let html = []
    itemKeys.forEach((key, pos) => {
      let item = items[key]
      let inputInfo = ''
      if (item.required) {
        inputInfo = 'Required. '
      } else {
        inputInfo = 'Optional. '
      }

      if (item.type === actionType.SLIDER) {
        inputInfo += `Values between ${item.min} - ${
          item.max
        } increment by ${item.step}`
        html.push(
          <InputSlider
            key={key}
            setFocus={pos === 0}
            name={key}
            placeholder={item.label}
            hasContent={item.value !== ''}
            min={item.min}
            max={item.max}
            step={item.step}
            onChange={this.handleInputChange}
            value={item.value}
            inputInfo={inputInfo}
          />
        )
      } else {
        //debugger
        inputInfo += `Text between ${item.min} and ${
          item.max
        } chars`
        html.push(
          <InputText
            key={key}
            setFocus={pos === 0}
            name={key}
            placeholder={item.label}
            hasContent={item.value !== ''}
            maxlength={item.max}
            onChange={this.handleInputChange}
            value={item.value}
            inputInfo={inputInfo}
          />
        )
      }
    })
    if (html.length === 0) {
      return (
        <div>
          <h1>Application error</h1>
          <p>
            Cannot provide required information. Please notify
            system administration about this error:&nbsp;
            <b>NodeModal.innerHtml missing!</b>
          </p>
        </div>
      )
    } else {
      return html
    }
  }

  render() {
    const headerProps = this.getHeaderProps()
    const navProps = {
      saveText: 'Save',
      onSave: this.onSave,
      canSave: this.state.valid,
      cancelText: 'Cancel',
      onCancel: this.onCancel
    }
    return (
      <ModalWindow>
        <ModalHeader {...headerProps} />
        <div className="Modal-body">{this.innerHtml()}</div>
        <ModalNav {...navProps} />
      </ModalWindow>
    )
  }
}

export default NodeModal
