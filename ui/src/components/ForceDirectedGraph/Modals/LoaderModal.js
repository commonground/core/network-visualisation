import React from 'react'

import ModalWindow from './ModalWindow'
import { LoaderPackman, LoaderBallTriangle } from '../Loaders'
import './LoaderModal.scss'
import * as TypeLoader from '../Helpers/cg.types'

/**
 * Modal with 2 typs of loaders. The default modal is 'packman'.
 * @param {String} props.type accepts string values 'packman' or 'ball-triangle'
 */
const LoaderModal = ({
  type = 'packman',
  title = 'Loading...'
}) => {
  let htmlContent = null
  switch (type) {
    case TypeLoader.LOADER_BALL_TRIANGLE:
      htmlContent = <LoaderBallTriangle />
      break
    case TypeLoader.LOADER_PACKMAN:
      htmlContent = <LoaderPackman />
      break
    default:
      htmlContent = <LoaderBallTriangle />
  }
  return (
    <ModalWindow>
      <div className="LoaderModal-frame">
        <p className="LoaderModal-message">{title}</p>
        {htmlContent}
      </div>
    </ModalWindow>
  )
}

export default LoaderModal
