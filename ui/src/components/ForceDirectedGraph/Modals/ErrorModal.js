import React from 'react'

import ModalWindow from './ModalWindow'
import './LoaderModal.scss'
import './ErrorModal.scss'
import BaseLineErrorRed from '../../../styles/icons/baseline-error_red-24px.svg'
/**
 * Modal with 2 typs of loaders. The default modal is 'packman'.
 * @param {String} props.type accepts string values 'packman' or 'ball-triangle'
 */
const ErrorModal = ({
  title = '500',
  svgIcon = BaseLineErrorRed,
  message = 'Server error'
}) => {
  return (
    <ModalWindow>
      <div className="ErrorModal-frame">
        <img
          className="ErrorModal-icon"
          src={svgIcon}
          alt="error icon"
        />
        <p className="ErrorModal-title">{title}</p>
        <p className="ErrorModal-message">{message}</p>
      </div>
    </ModalWindow>
  )
}

export default ErrorModal
