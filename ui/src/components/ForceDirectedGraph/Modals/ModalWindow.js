import React from 'react'

import './ModalWindow.scss'

const ModalWindow = props => {
  return (
    <section className="Modal-backdrop">
      <div className="Modal-window">{props.children}</div>
    </section>
  )
}

export default ModalWindow
