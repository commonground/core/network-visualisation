import React, { Component } from 'react'

import * as actionType from '../Helpers/cg.types'
import cgu from '../Helpers/cg.util'
import BaseLineKey from '../../../styles/icons/baseline-vpn_key-24px.svg'
import ModalWindow from './ModalWindow'
import ModalHeader from './ModalHeader'
import ModalNav from './ModalNav'

import { InputText, InputPassword } from '../Inputs'

class LoginModal extends Component {
  state = {
    username: '',
    password: ''
  }
  //list of required fields
  required = ['password', 'username']
  onSave = () => {
    this.props.onModalAction({
      type: actionType.SIGN_IN,
      payload: {
        username: this.state.username,
        password: this.state.password
      }
    })
  }
  onCancel = () => {
    this.props.onModalAction({
      type: actionType.CANCEL_ACTION
    })
  }
  handleInputChange = event => {
    const { name, value } = event.target
    this.setState({
      [name]: value,
      valid: cgu.isModalValid(
        this.state,
        this.required,
        event.target
      )
    })
  }
  handleEnterKey = event => {
    if (event.charCode === 13) {
      this.onSave()
    }
  }
  render() {
    const headerProps = {
      title: 'Sign in',
      img: {
        src: BaseLineKey,
        alt: 'add product'
      }
    }
    const navProps = {
      saveText: 'Login',
      onSave: this.onSave,
      canSave: this.state.valid,
      cancelText: 'Cancel',
      onCancel: this.onCancel
    }
    return (
      <ModalWindow>
        <ModalHeader {...headerProps} />
        <div className="Modal-body">
          <InputText
            setFocus={true}
            name="username"
            placeholder="User"
            hasContent={this.state.username !== ''}
            maxlength="20"
            onChange={this.handleInputChange}
            value={this.state.username}
            inputInfo="Required. Maximum length of 20 chars."
          />
          <InputPassword
            name="password"
            placeholder="Password"
            hasContent={this.state.password !== ''}
            maxlength="50"
            onChange={this.handleInputChange}
            onKeyPress={this.handleEnterKey}
            value={this.state.password}
            inputInfo="Required. Maximum length of 50 chars."
          />
        </div>
        <ModalNav {...navProps} />
      </ModalWindow>
    )
  }
}

export default LoginModal
