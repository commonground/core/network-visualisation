import React from 'react'

const ModalHeader = props => {
  return (
    <header className="Modal-title">
      {props.title}
      <img src={props.img.src} alt={props.img.alt} />
    </header>
  )
}

export default ModalHeader
