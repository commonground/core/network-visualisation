/**
 * Input types for modals and configration page
 */
export { default as InputColor } from './InputColor'
export { default as InputNumber } from './InputNumber'
export { default as InputSlider } from './InputSlider'
export { default as InputText } from './InputText'
export { default as InputPassword } from './InputPassword'
export { default as InputSelect } from './InputSelect'
export {
  default as InputCustomCheckbox
} from './InputCustomCheckbox'
