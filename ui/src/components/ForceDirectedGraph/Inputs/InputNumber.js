import React from 'react'

import './InputNumber.scss'

/**
 * Input type text with label effect
 * @param {Object} props
 * @param {String} props.label
 * @param {String} props.name
 * @param {Number} props.min
 * @param {Number} props.max
 * @param {Number} props.value
 * @param {Function} props.onChange
 */
const InputNumber = props => {
  return (
    <div className="InputNumber">
      <div className="InputNumber-effect">
        <input
          name={props.name}
          className={`effect-16 ${
            props.hasContent ? ' has-content' : null
          }`}
          type="number"
          placeholder=""
          min={props.min}
          max={props.max}
          onChange={props.onChange}
          value={props.value}
          autoFocus={props.setFocus || false}
        />
        <label className="InputNumber-placeholder">
          {props.placeholder}
        </label>
        <span className="focus-border" />
      </div>
      <div className="input-info">{props.inputInfo}</div>
    </div>
  )
}

export default InputNumber
