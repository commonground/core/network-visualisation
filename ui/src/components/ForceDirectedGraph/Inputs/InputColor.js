import React from 'react'

import './InputColor.scss'
// TODO! NOT COMPLETED
/**
 * Input type text with label effect
 * @param {Object} props
 * @param {String} props.label
 * @param {Function} props.onChange
 */
const InputColor = props => {
  return (
    <div className="InputColor">
      <input
        type="color"
        className="InputColor-button"
        name={props.name}
        value={props.color}
        onChange={props.onChange}
        autoFocus={props.setFocus || false}
      />
      <div className="InputColor-info">{props.color}</div>
    </div>
  )
}

export default InputColor
