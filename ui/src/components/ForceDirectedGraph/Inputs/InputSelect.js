import React from 'react'

import './InputSelect.scss'

const InputSelect = props => {
  //debugger
  let innerHtml = props.options.map((opt, id) => {
    return (
      <option key={opt.value} value={opt.value} data-id={id}>
        {opt.label}
      </option>
    )
  })
  return (
    <div className="InputSelect">
      <div className="InputSelect-label">{props.label}</div>
      <select
        name={props.name}
        className="InputSelect-select"
        onChange={props.onChange}
        value={props.value}>
        {innerHtml}
      </select>
    </div>
  )
}

export default InputSelect
