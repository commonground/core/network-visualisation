import React, { Component } from 'react'

import './InputCustomCheckbox.scss'

class InputCustomCheckbox extends Component {
  value = false
  toggleState = () => {
    const { name } = this.props
    this.value = !this.value
    this.props.onChange({
      target: {
        name: name,
        value: this.value
      }
    })
  }
  handleKeyPress = event => {
    if (event.charCode === 13) {
      this.toggleState()
    }
  }
  render() {
    const { name, placeholder, value } = this.props
    this.value = value
    return (
      <div
        onClick={this.toggleState}
        onKeyPress={this.handleKeyPress}
        className="InputCustomCheckbox"
        title="Click to change status"
        tabIndex="0">
        <label
          className="InputCustomCheckbox-label"
          htmlFor={name}>
          {placeholder}
        </label>
        <div className="InputCustomCheckbox-check">
          {value ? 'Yes' : 'No'}
        </div>
      </div>
    )
  }
}

export default InputCustomCheckbox
