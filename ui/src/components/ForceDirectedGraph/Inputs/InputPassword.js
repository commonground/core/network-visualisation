import React from 'react'

import './InputText.scss'

/**
 * Input type text with label effect
 * @param {Object} props
 * @param {String} props.label
 * @param {Function} props.onChange
 */
const InputPassword = props => {
  return (
    <div className="InputText">
      <div className="InputText-effect">
        <input
          name={props.name}
          className={`effect-16 ${
            props.hasContent ? ' has-content' : null
          }`}
          type="password"
          placeholder=""
          maxLength={props.maxlength}
          onChange={props.onChange}
          onKeyPress={props.onKeyPress}
          value={props.value}
          autoFocus={props.setFocus || false}
        />
        <label className="InputText-placeholder">
          {props.placeholder}
        </label>
        <span className="focus-border" />
      </div>
      <div className="input-info">{props.inputInfo}</div>
    </div>
  )
}

export default InputPassword
