import React from 'react'

import './InputSlider.scss'

/**
 * Input type text with label effect
 * @param {Object} props
 * @param {String} props.label
 * @param {Function} props.onChange
 */
const InputSlider = props => {
  return (
    <div className="InputSlider">
      <div className="InputSlider-effect">
        <label className="InputSlider-placeholder">
          <span>{props.placeholder}</span>
          <span className="InputSlider-value">
            {props.value}
          </span>
        </label>
        <input
          name={props.name}
          className="InputSlider-range"
          type="range"
          placeholder=""
          min={props.min}
          max={props.max}
          step={props.step}
          onChange={props.onChange}
          value={props.value}
          autoFocus={props.setFocus || false}
        />
      </div>
      <div className="input-info">{props.inputInfo}</div>
    </div>
  )
}

export default InputSlider
