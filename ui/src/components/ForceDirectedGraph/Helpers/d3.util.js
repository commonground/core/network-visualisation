// global d3
// third party dependencies
import * as d3 from 'd3'
import invertColor from 'invert-color'
// local dependencies
import * as actionType from './cg.types'
import { defaultSimCfg, defaultScaleRange } from './cg.config'
import cgc from './cg.config'
import { decodeData } from '../Graphql/gqlHelpers'
import AuthSvc from '../../AuthRoute/AuthSvc'
//--------------------------------
// D3 CONSTANTS
const LINKS_GROUP_CLASS = 'cg-links-group'
const NODES_GROUP_CLASS = 'cg-nodes-group'
const LINK_CLASS = 'cg-node-link'
const NODE_CLASS = 'cg-node-item'
const SOURCE_NODE_CLASS = 'source-node'
const VALID_LINK_CLASS = 'valid-link'
const INVALID_LINK_CLASS = 'invalid-link'
const PINNED_NODE = 'pinned-node'
const SELECTED_CLASS = 'selected'
const DEFAULT_COLOR = '#666'
//--------------------------------
// INTERNAL VARIABLES
let svg
let zoom
let scale = 1
let dataNodes = []
let dataLinks = []
let gNodes
let gLinks
let mergedNodes
let mergedLinks
let simulation
let simCfg = {}
/**
 * Calculating chart (svg) size based on parent dimenensions.
 * This function is used to create svg element to fits the
 * parent element.
 * @param {HTMLElement} parent
 * @returns {Object}
 * {
 *  width: Number,
 *  height: Number
 * }
 */
export function getChartSize(parent) {
  const { minWidth, minHeight } = defaultSimCfg.svg
  let w = null,
    h = null,
    innerMargin = 16

  if (parent) {
    let ps = window.getComputedStyle(parent, null)
    w =
      parseInt(ps.width) -
      (parseInt(ps.paddingLeft) +
        parseInt(ps.paddingRight) +
        innerMargin)
    //minimum width
    if (w < minWidth) w = minWidth
    h =
      parseInt(ps.height) -
      (parseInt(ps.paddingTop) +
        parseInt(ps.paddingBottom) +
        innerMargin)
    // minimum height of 400 px
    if (h < minHeight) h = minHeight
  }
  let size = {
    width: w,
    height: h
  }
  return size
}
/**
 * Create SVG element and groups for links and nodes.
 * @param {HTMLElement} parent parent element
 * @param {Object} size chart size
 * @returns {Object} { svg, gLinks, gNodes }
 */
export const createSvg = (parent, size, fn) => {
  let el = d3.select(parent)
  //remove existing svg (if any)
  el.selectAll('svg').remove()
  //create new svg
  svg = el
    .append('svg')
    .attr('width', size.width)
    .attr('height', size.height)
    .on('contextmenu', function(d) {
      if (
        AuthSvc.tokenContainsUserRole(actionType.ROLE_ADMIN)
      ) {
        //prevent default events
        d3.event.preventDefault()
        d3.event.stopPropagation()
        //callback function
        fn.contextmenu({
          event: d3.event,
          item: this,
          data: {
            type: actionType.SVG_CONTEXT
          }
        })
      }
    })

  //create zoom svg group
  let gZoom = svg.append('g').attr('class', 'zoom-group')
  //create zoom defs
  zoom = d3
    .zoom()
    //min and max zoom scale
    .scaleExtent(getZoomScale())
    .on('zoom', function() {
      //save scale
      scale = d3.event.transform.k
      gZoom.attr('transform', d3.event.transform)
    })
  //add zoom to svg
  svg.call(zoom)

  //add group for links
  gLinks = gZoom.append('g').attr('class', LINKS_GROUP_CLASS)
  //add group for nodes
  gNodes = gZoom.append('g').attr('class', NODES_GROUP_CLASS)

  return {
    svg,
    gLinks,
    gNodes
  }
}
/**
 * Validate links agains dataNodes.
 * @param {Array} dataLinks
 * @param {String} dataLinks.source source node id
 * @param {String} dataLinks.target target node id
 * @param {Array} dataNodes
 * @param {String} dataNodes.id node id
 * @return {Array} validLinks
 */
export function validateDataLinks(dataLinks, dataNodes) {
  let nodesId = dataNodes.map(node => node.id)
  let validLinks = []
  dataLinks.forEach(link => {
    if (
      nodesId.indexOf(link.source) !== -1 &&
      nodesId.indexOf(link.target) !== -1
    ) {
      validLinks.push(link)
    }
  })
  return validLinks
}
/**
 * Clear all nodes and links.
 * Use this function when simulations settings are changed
 * to remove all svg elements. Then call updateLinks and
 * updateNodes methods to redraw the visualization.
 */
export function clearAll() {
  //remove all link elements from svg
  svg.selectAll(`.${LINK_CLASS}`).remove()
  //remove all node elements from svg
  svg.selectAll(`.${NODE_CLASS}`).remove()
}
/**
 * Extract zoom min and max from configuration
 */
function getZoomScale() {
  let { zoomMin, zoomMax } = cgc.getConfigurationSection(
    'simulation'
  )
  let zoom = []
  if (zoomMin) {
    zoom = [zoomMin.value, zoomMax.value]
  } else {
    //defaults
    zoom = [0.5, 3]
  }
  return zoom
}
/**
 * Reset zoom to start values
 */
export function resetZoom() {
  svg.call(
    zoom.transform,
    d3.zoomIdentity.translate(0, 0).scale(1)
  )
}
/**
 * Creates or updates links between nodes
 * @param {Object} svg d3 reference to svg object
 * @param {Object} data array of links definitions
 * @param {String} data.source node id
 * @param {String} data.target node id
 * @param {Object} fn callback functions for interaction
 */
export function updateLinks(data, fn) {
  //debugger
  if (data === undefined) return
  //save data locally
  dataLinks = data
  //select all svg elements with specific class name
  //and join them with received data array
  let svgLinks = gLinks
    .selectAll(`.${LINK_CLASS}`)
    //track data by id
    .data(dataLinks, d => {
      //debugger
      //return `${d.source.id}-${d.target.id}`
      return d.id
    })

  //remove svg links not matching the data
  svgLinks.exit().remove()

  //add new svg link elements and merge
  let newLinks = svgLinks
    //add new links
    .enter()
    .append('line')
    .attr('stroke-width', simCfg.strokeWidth.value)
    .attr('stroke', simCfg.strokeColor.value)
    .attr('class', LINK_CLASS)
    //position
    .attr('x1', function(d) {
      return d.x1
    })
    .attr('y1', function(d) {
      return d.y1
    })
    .attr('x2', function(d) {
      return d.x2
    })
    .attr('y2', function(d) {
      return d.y2
    })
    .on('mouseover', function(d) {
      if (
        AuthSvc.tokenContainsUserRole(actionType.ROLE_ADMIN)
      ) {
        this.classList.add(SELECTED_CLASS)
      }
    })
    .on('mouseout', function(d) {
      this.classList.remove(SELECTED_CLASS)
    })
    .on('click', function(d) {
      if (
        AuthSvc.tokenContainsUserRole(actionType.ROLE_ADMIN)
      ) {
        fn.click({
          type: actionType.LINK_CONTEXT,
          item: this,
          data: d
        })
      }
    })
    .on('contextmenu', function(d, i) {
      //prevent default events
      d3.event.preventDefault()
      d3.event.stopPropagation()
      /**
       * callback function passing following data
       * e: mouseEvent, c: circle object, d: data, i: circle instance
       */
      fn.contextmenu({
        type: actionType.LINK_CONTEXT,
        event: d3.event,
        item: this,
        data: d
      })
    })

  //merge new links with existing links
  mergedLinks = newLinks.merge(svgLinks)
  return mergedLinks
}
/**
 * Creates circle nodes in svg using d3 svg reference provided and
 * binds mouse events for provided function
 * @param {Object} data all node data available
 * @param {String} data.type str
 * @param {Object} fn callback functions for interaction
 * @param {Function} fn.click callback functions on node click
 * @param {Function} fn.contextmenu callback functions on right mouse click
 */
export const updateNodes = (data, fn) => {
  if (data === undefined) return
  dataNodes = data
  //select all svg elements with specific class name
  //and join them with received data array
  let svgNodes = gNodes
    .selectAll(`.${NODE_CLASS}`)
    //track nodes by id
    .data(data, d => d.id)

  //remove not maching nodes
  svgNodes.exit().remove()

  //mark existing nodes
  //to valid or invalid
  svgNodes.each(function(d) {
    let el = this
    if (d.state) {
      switch (d.state) {
        case actionType.SOURCE_NODE:
          el.classList.add(SOURCE_NODE_CLASS)
          break
        case actionType.TARGET_NODE:
          el.classList.add(VALID_LINK_CLASS)
          break
        default:
          el.classList.add(INVALID_LINK_CLASS)
      }
    } else {
      el.classList.remove(SOURCE_NODE_CLASS)
      el.classList.remove(VALID_LINK_CLASS)
      el.classList.remove(INVALID_LINK_CLASS)
    }

    if (el.classList.contains(PINNED_NODE) && !d.fixed) {
      //pinned node class is on the element
      //while the fixed prop is removed (data reload)
      let pos = el.transform.baseVal.consolidate()
      if (pos.matrix.e) {
        //if we can recover position from SVG element
        //we keep fixed node
        d.fixed = true
        d.fx = pos.matrix.e
        d.fy = pos.matrix.f
      } else {
        //else we remove pinnen class
        el.classList.remove(PINNED_NODE)
      }
    }
  })
  //recalculate circle size on update
  svgNodes.select('circle').attr('r', d => {
    //add radius
    //debugger
    d['radius'] = calculateRadius(d)
    return d['radius']
  })
  //recalculate text props
  svgNodes
    .select('text')
    .attr('font-size', d => {
      return Math.round(d.radius * 0.55)
    })
    .text(d => `${d.label}`)

  //add new nodes
  let newNodes = svgNodes
    .enter()
    .append('g')
    .attr('class', NODE_CLASS)
    .attr('transform', d => {
      //set initial position
      //middle of the svg
      let pos = getNewNodePosition(d)
      d.x = pos.x
      d.y = pos.y
      return `translate(${d.x},${d.y})`
    })
    .on('mouseover', function(d) {
      if (d.state && d.state === actionType.TARGET_NODE) {
        this.classList.add(SELECTED_CLASS)
      } else {
        this.classList.remove(SELECTED_CLASS)
        d3.select(this).attr('cursor', 'move')
      }
    })
    .on('mouseout', function(d) {
      if (!d.state) {
        d3.select(this).attr('cursor', 'default')
      } else {
        this.classList.remove(SELECTED_CLASS)
      }
    })
    .on('contextmenu', function(d, i) {
      //prevent default events
      d3.event.preventDefault()
      d3.event.stopPropagation()
      //stop simulation
      simulation.stop()
      /**
       * callback function passing following data
       * e: mouseEvent, c: circle object, d: data, i: circle instance
       */
      fn.contextmenu({
        type: actionType.NODE_CONTEXT,
        event: d3.event,
        item: this,
        data: d
      })
    })

  //append circle to new node
  newNodes
    .append('circle')
    .attr('r', d => {
      //add radius
      //debugger
      d['radius'] = calculateRadius(d)
      return d['radius']
    })
    .attr('class', d => {
      // debugger
      return `cg-node-circle cg-node-type-${d.type.toLowerCase()}`
    })
    .attr('fill', d => {
      return getNodeFill(d)
    })
    .attr('stroke', simCfg.strokeColor.value)
    .attr('stroke-width', simCfg.strokeWidth.value)
    .on('click', function(d) {
      // console.log('Node clicked...', d.id)
      if (d.state && d.state === actionType.TARGET_NODE) {
        //console.log('Link this node...', d)
        fn.click({
          event: d3.event,
          item: this,
          data: d
        })
      }
    })

  //append text to new node
  newNodes
    .append('text')
    .attr('class', 'cg-node-text')
    .attr('text-anchor', 'middle')
    .attr('dominant-baseline', 'central')
    .attr('font-size', d => {
      return Math.round(d.radius * 0.55)
    })
    .attr('fill', d => {
      let c = ''
      try {
        c = invertColor(d.color, true)
      } catch (e) {
        //dark is default
        c = '#212121'
      }
      return c
    })
    .text(d => `${d.label}`)

  //addDragFeatureToNewNodes(newNodes)
  //add drag capability to new nodes
  newNodes.call(
    d3
      .drag()
      .on('start', onNodeDragStart)
      .on('drag', onNodeDrag)
      .on('end', onNodeDragEnd)
  )
  //merge new nodes with existing nodes
  mergedNodes = newNodes.merge(svgNodes)
  return mergedNodes
}

function onNodeDragStart(d) {
  if (isDragEnabled(d)) {
    const alpha = getAlphaValues('start')
    simulation
      .alphaTarget(alpha.target)
      .alpha(alpha.value)
      .restart()

    d.fx = d.x
    d.fy = d.y
  }
}

function onNodeDrag(d) {
  //debugger
  if (isDragEnabled(d)) {
    d.fx = d3.event.x
    d.fy = d3.event.y
  }
}

function onNodeDragEnd(d) {
  //debugger
  if (isDragEnabled(d)) {
    if (!d.fixed) {
      d.fx = null
      d.fy = null
    }
    const alpha = getAlphaValues('stop')
    // restart with standard sim values
    simulation
      .alpha(alpha.value)
      .alphaTarget(alpha.target)
      .alphaMin(alpha.min)
      .restart()
  } else {
    simulation.stop()
  }
}

function isDragEnabled(d) {
  if (d.state) {
    //console.log('drag...ignored...linking in progress...', d.id)
    return false
  } else {
    return true
  }
}

function getAlphaValues(dragType) {
  //debugger
  let alpha = {
    target: simCfg.alphaTarget.value,
    value: simCfg.alphaValue.value,
    min: simCfg.alphaMin.value
  }
  if (dragType === 'start') {
    //debugger
    //NOTE! alphaTarget needs to be slightly HIGHER than alpha value
    //in order for sim to NEVER reach the target, so the simulation
    //and the drag event are performed as long as user needs. Then
    //on drag end alphaTarget and alpha values are returned to default
    // (normal) values where alphaTarget is lower than alpha.
    alpha.target = alpha.value + 0.01
    if (alpha.target < 0) alpha.target = 0
    return alpha
  } else {
    // back to default values
    // NOTE! the simulation is back to normal state.
    // During testing use of other alpha values has not
    // proven to be beneficial. Some unexpected behaviour
    // of nodes is observed when alphaTarget is manipulated
    return alpha
  }
}

/**
 * Calculate circle radius based specific values
 * @param {Object} d D3 data object
 */
function calculateRadius(d) {
  //scaling
  let area,
    scale,
    r,
    value = 1,
    defaultNodeRadius = simCfg.defaultNodeSize.value

  try {
    const { typeSize, typeProps } = cgc.getConfigurationSection(
      'nodeProps'
    )
    let size = typeSize[d.type]
    if (size.type === actionType.CONSTANT_NODE_SIZE) {
      return defaultNodeRadius
    } else if (d.dataB64) {
      let nodeData = decodeData(d.dataB64)
      if (nodeData.hasOwnProperty(size.value)) {
        //get prop min/max
        let { min, max } = typeProps[d.type][size.value]
        //get node value
        value = nodeData[size.value]
        //scale node values
        //within specific boundaries
        scale = d3
          .scaleLinear()
          .domain([min, max])
          .range([defaultScaleRange.min, defaultScaleRange.max])
        //scaled value should represent circle area
        area = scale(value)
        //calculate radius from scaled area
        r = Math.round(Math.sqrt(area / Math.PI))
        return r
      } else {
        return defaultNodeRadius
      }
    } else {
      return defaultNodeRadius
    }
  } catch (e) {
    console.error(e)
    return defaultNodeRadius
  }
}

function getNodeFill(d) {
  try {
    const nodeTypes = cgc.getConfigurationSection('nodeTypes')
    let nodeType = nodeTypes[d.type]
    d.color = nodeType.color
    return nodeType.color
  } catch (e) {
    return DEFAULT_COLOR
  }
}
/**
 * Save configuration parameters locally. Params are received at init
 * or when changed by ForceDirectedGraph.js
 * @param {Object} config configuration object
 * @param {Object} config.linkDistance distance between links
 * @param {Object} config.chargeStrength charge between nodes, positive attract - negative repel
 * @param {Object} config.collideStrength > 1 doesn't allow overlaying nodes
 * @param {Object} config.alphaValue value 0 to 1, default 0.5
 * @param {Object} config.alphaMin value 0 to 1, it must be < alphaValue, default 0.07
 * @param {Object} config.alphaTarget value 0 to 1, it must be < alphaMin, default 0.05
 */
export function saveSimulationParameters(config) {
  let keys = Object.keys(config)
  keys.forEach(key => {
    let val = config[key]
    simCfg[key] = val
  })
}

/**
 * Initialize simulation. This happens only once at the start.
 * @param {Array} dataNodes array of data nodes
 * @param {Object} config simulation configuration object
 */
export function initSimulation(config) {
  const {
    linkDistance = simCfg.linkDistance,
    chargeStrength = simCfg.chargeStrength,
    svg = simCfg.svg,
    alphaTarget = simCfg.alphaTarget,
    alphaMin = simCfg.alphaMin,
    alphaValue = simCfg.alphaValue
  } = config

  simulation = d3
    .forceSimulation([])
    .force('link', d3.forceLink().distance(linkDistance.value))
    .force('collide', d3.forceCollide())
    .force(
      'charge',
      d3.forceManyBody().strength(chargeStrength.value)
    )
    .force(
      'center',
      d3.forceCenter(svg.width / 2, svg.height / 2)
    )
    .alpha(alphaValue.value)
    .alphaTarget(alphaTarget.value)
    .alphaMin(alphaMin.value)
    .on('tick', onSimulationTick)
    .stop()
  //log sim params
  //logSimulationParams('initSimulation')
  //log simulation end
  // simulation.on('end', () => {
  //   logSimulationParams('onSimulationEnd')
  // })
  saveSimulationParameters(config)
  return simulation
}

function onSimulationTick() {
  //let start = true
  let sim = this
  try {
    // POSITIONING NODE GROUPS
    mergedNodes.attr('transform', d => {
      //calculate position not to be
      //outside frame
      let pos = getNewNodePosition(d)
      //update d values to be
      //used with links simulation
      d.x = pos.x
      d.y = pos.y
      return `translate(${d.x},${d.y})`
    })
    //POSITION LINKS
    mergedLinks
      .attr('x1', function(d) {
        return d.source.x
      })
      .attr('y1', function(d) {
        return d.source.y
      })
      .attr('x2', function(d) {
        return d.target.x
      })
      .attr('y2', function(d) {
        return d.target.y
      })
  } catch (e) {
    console.error(e)
    sim.stop()
  }
}
/**
 * Get node position (x,y) during the simulation run.
 * Ensures that node pos (x,y) stays within svg size.
 * @param {Object} d d3 node object
 * @returns Object{ x: number, y: number }
 */
function getNewNodePosition(d) {
  const { svg } = simCfg
  //initial values
  let x = d.x
  let y = d.y
  if (d.fixed) {
    if (d.fx) {
      return {
        x: d.fx,
        y: d.fy
      }
    } else {
      console.warn('getNewNodePosition..fixed node missing fx')
    }
  } else {
    //limit only when scale is 1
    if (scale === 1) {
      x = stayWithinFrame(d.x, d.radius, svg.width)
      y = stayWithinFrame(d.y, d.radius, svg.height)
    }
    return {
      x,
      y
    }
  }
}

function stayWithinFrame(pos, radius, max, min = 0) {
  let margin = 16
  let minWithMargin = min + margin + radius
  let maxWithMargin = max - margin - radius

  if (isNaN(pos) === true) return max / 2

  if (pos < minWithMargin) {
    return minWithMargin
  } else if (pos > maxWithMargin) {
    return maxWithMargin
  } else {
    return pos
  }
}

/**
 * Refresh existing D3 simulation object after data is changed.
 * This is third step of the complete update sequence.
 * The three steps are: updateLinks, updateNodes and updateSimulation
 * @param {Object} config
 * @param {Number} config.linkDistance
 * @param {Number} config.collideStrength
 * @param {Number} config.alpha
 */
export function updateSimulation(config = {}) {
  let {
    linkDistance = simCfg.linkDistance,
    collideStrength = simCfg.collideStrength,
    alphaValue = simCfg.alphaValue,
    alphaTarget = simCfg.alphaTarget,
    alphaMin = simCfg.alphaMin
  } = config
  // debugger
  try {
    //update simulation nodes
    simulation.nodes(dataNodes)
    //update simulation links
    simulation
      .force('link')
      .id(d => {
        //return nodes id as link reference
        return d.id
      })
      .links(dataLinks)
      .distance(d => {
        return parseInt(linkDistance.value)
      })
    //update force collide
    simulation.force('collide').radius(d => {
      return d.radius * collideStrength.value
    })
    validateSimParams(
      alphaValue.value,
      alphaMin.value,
      alphaTarget.value
    )
    //restart sim from alpha value
    simulation
      .alpha(alphaValue.value)
      .alphaTarget(alphaTarget.value)
      .alphaMin(alphaMin.value)
      .restart()
    //log simulation params
    //logSimulationParams('updateSimulation')
    saveSimulationParameters(config)
    return simulation
  } catch (e) {
    console.error('d3.updateSimulation...', e)
  }
}
/**
 * Restarts the simulation with last saved parameters
 * alphaValue, alphaTarget and alphaMin
 * If alphaVal is provided it wil be used instead of
 * default values defined in SimSettings.
 * @param {Number} alphaVal, number between 0.01 and 1
 */
export function restartSimulation(alphaVal) {
  //get default values
  let { alphaValue, alphaTarget, alphaMin } = simCfg
  //create restart values
  let aValue = alphaValue.value,
    aTarget = alphaTarget.value,
    aMin = alphaMin.value
  //check parameter provided
  if (alphaVal && alphaVal !== aValue) {
    aValue = alphaVal
  }
  validateSimParams(aValue, aMin, aTarget)
  simulation
    .alpha(aValue)
    .alphaTarget(aTarget)
    .alphaMin(aMin)
    .restart()
  //log simulation parameters
  //logSimulationParams('restartSimulation')
}
/**
 * Sets number of ticks (runs) in the simulation
 * and restarts simulation to perfom these ticks.
 * By default 20 itteractions are called.
 * I tried it but did not found this feature t
 * crate smooth transitions.
 */
export function tickSimulation(steps = 20) {
  //console.log('tickSimulation...', steps)
  simulation.tick(steps)
  simulation.restart()
}
/**
 * Stop simulation (loop)
 */
export function stopSimulation() {
  // console.log('stopSimulation...')
  simulation.stop()
}
/* eslint-disable */
/**
 * Validate simulation parameters. The
 * related alpha values need to follow
 * specific requirements for sim to work
 * and to be able to complete simulation loop
 * @param {*} alpha
 * @param {*} alphaMin
 * @param {*} alphaTarget
 */
function validateSimParams(alpha, alphaMin, alphaTarget) {
  let msg = [];
  if (alpha < alphaTarget) {
    msg.push(`alpha < alphaTarget...${alpha} < ${alphaTarget}`);
  }
  if (alphaMin < alphaTarget) {
    msg.push(`alphaMin < alphaTarget...${alphaMin} < ${alphaTarget}`);
  }
  if (alpha < alphaMin) {
    msg.push(`alpha < alphaMin...${alpha} < ${alphaMin}`);
  }
  if (msg.length > 0) {
    console.group("validateSimParams");
    msg.forEach(item => {
      console.warn(item);
    });
    console.groupEnd();
  }
}
function logSimulationParams(source) {
  let alpha = {
    value: simulation.alpha(),
    target: simulation.alphaTarget(),
    min: simulation.alphaMin()
  };
  console.group(`logSimulationParams: ${source}`);
  console.log("alpha...", alpha.value);
  console.log("alphaTarget...", alpha.target);
  console.log("alphaMin...", alpha.min);
  console.groupEnd();
}
/* eslint-enable */

export function updateSimulationNodes() {
  //update simulation nodes
  simulation.nodes(dataNodes)
}
/**
 * Updates nodes and links at the same time
 * because update of links requires nodeData
 * to perform update.
 */
export function updateSimulationNodesAndLinks() {
  const { linkDistance } = simCfg
  //it requires nodes to update
  // links properly
  simulation.nodes(dataNodes)
  //update links
  simulation
    .force('link')
    .id(d => {
      //return nodes id as link reference
      return d.id
    })
    .links(dataLinks)
    .distance(d => {
      return (
        linkDistance.value + d.source.radius + d.target.radius
      )
    })
}
/**
 * Toggles class name for node that
 * is pinned. It is called from
 * ForceDirectedGraph.js
 * @param {*} node
 */
export function togglePinnedNode(node) {
  if (node.data.fixed) {
    node.data.fixed = !node.data.fixed
  } else {
    node.data.fixed = true
  }
  if (node.data.fixed === true) {
    node.data.fx = node.data.x
    node.data.fy = node.data.y
    node.item.classList.add(PINNED_NODE)
  } else {
    node.data.fx = null
    node.data.fy = null
    node.item.classList.remove(PINNED_NODE)
    restartSimulation()
  }
}
/**
 * Change the links opacity during linking
 * proces.
 * @param {Number} opacity
 */
export function setLinksOpacity(opacity = 1) {
  gLinks.attr('opacity', opacity)
}

export default {
  getChartSize,
  createSvg,
  updateNodes,
  togglePinnedNode,
  validateDataLinks,
  updateLinks,
  initSimulation,
  updateSimulation,
  updateSimulationNodesAndLinks,
  restartSimulation,
  tickSimulation,
  stopSimulation,
  setLinksOpacity,
  clearAll,
  resetZoom
}
