/**
 * Common Ground Network utility functions
 * for handling the bussiness logic
 * v.0.1.0
 */
import * as actionType from './cg.types'
import ContextMenu from '../ContextMenu/ContextMenu'
import BaseLineDelete from '../../../styles/icons/baseline-delete-24px.svg'
import BaseLineEdit from '../../../styles/icons/baseline-edit-24px.svg'
import BaseLineLinkNode from '../../../styles/icons/baseline-linear_scale-24px.svg'
import BaseLinePinNode from '../../../styles/icons/baseline-gps_not_fixed-24px.svg'
import BaseLineClear from '../../../styles/icons/baseline-clear-24px.svg'
import BaseLineDone from '../../../styles/icons/baseline-done-24px.svg'

import AuthSvc from '../../AuthRoute/AuthSvc'
import cgc from './cg.config'
/**
 * Returns array of menu items used by ContextMenu based on node type.
 * Callback function is invoked when menu item is clicked returing the
 * menu option and the node data from D3.
 * @param {Object} node d3 node object
 * @param {Function} fn callback function invoked when menu item is clicked returing the
 * menu option and the node data from D3.
 * @returns [{
 *  text: string;
 *  icon: string;
 *  enabled: boolean;
 *  events:{
 *    click: e => {fn(type,node)}
 *  }
 * }] of menu item objects for ContextMenu
 */
export function getNodeContextMenu(node, fn) {
  //debugger
  const { type, state } = node.data
  let menuItems
  if (state) {
    menuItems = getLinkNodeItems(node, fn)
  } else {
    //debugger
    if (AuthSvc.tokenContainsUserRole('admin')) {
      if (type === actionType.SVG_CONTEXT) {
        menuItems = getSvgMenuItems(node, fn)
      } else {
        menuItems = getNodeMenuItems(node, fn)
      }
    } else {
      menuItems = getViewerMenuItems(node, fn)
    }
  }
  return menuItems
}

function getLinkNodeItems(node, fn) {
  const { state } = node.data
  let menuItems = [
    {
      text: 'Cancel linking',
      icon: `<img class="cm-svg-icon" src="${BaseLineClear}" alt="add person"/>`,
      enabled: true,
      events: {
        click: e => {
          fn({
            type: actionType.CANCEL_LINK_PROCESS,
            node
          })
        }
      }
    }
  ]
  if (state === actionType.TARGET_NODE) {
    menuItems.push({
      text: 'Complete linking',
      icon: `<img class="cm-svg-icon" src="${BaseLineDone}" alt="add person"/>`,
      enabled: true,
      events: {
        click: e => {
          fn({
            type: actionType.COMPLETE_LINK_PROCESS,
            node
          })
        }
      }
    })
  }
  return menuItems
}

function getNodeMenuItems(node, fn) {
  const nodeTypes = cgc.getConfigurationSection('nodeTypes')
  let menuItems = []
  let keys = Object.keys(nodeTypes)
  //debugger
  // ADD NODE SECTION
  keys.forEach(key => {
    let item = nodeTypes[key]
    let action = {
      type: actionType.ADD_NODE_AND_LINK,
      payload: {
        nodeType: item,
        nodeProps: getNodeProps(key),
        nodeSize: getNodeSize(key),
        node
      }
    }
    let menuItem = {
      text: `Add ${item.label.toLowerCase()}`,
      icon: `<div class="svg-menu-type-icon" style="${'background-color:' +
        item.color}"></div>`,
      enabled: getLinkingRule(node.data.type, key),
      events: {
        click: e => {
          fn(action)
        }
      }
    }
    menuItems.push(menuItem)
  })
  //DIVIDER
  menuItems.push({
    // This item is a divider (shows only gray line, no text etc.)
    type: ContextMenu.DIVIDER
  })
  //EDIT NODE
  menuItems.push({
    text: `Edit ${node.data.label}`,
    icon: `<img class="cm-svg-icon" src="${BaseLineEdit}" alt="edit node"/>`,
    enabled: true,
    events: {
      click: e => {
        fn({
          type: actionType.EDIT_NODE,
          payload: {
            nodeType: getNodeType(node.data.type),
            nodeProps: getNodeProps(node.data.type),
            node
          }
        })
      }
    }
  })
  // LINK NODE
  menuItems.push({
    text: `Link ${node.data.label}`,
    icon: `<img class="cm-svg-icon" src="${BaseLineLinkNode}" alt="link node"/>`,
    enabled: true,
    events: {
      click: e => {
        fn({
          type: actionType.START_LINK_PROCESS,
          node
        })
      }
    }
  })
  // PIN/UNPIN NODE
  menuItems.push({
    text: `${
      node.data.fixed
        ? 'Unpin ' + node.data.label
        : 'Pin ' + node.data.label
    }`,
    icon: `<img class="cm-svg-icon" src="${BaseLinePinNode}" alt="toggle node"/>`,
    enabled: node.data.fixed || true,
    events: {
      click: e => {
        fn({
          type: actionType.TOGGLE_PINNED_NODE,
          node
        })
      }
    }
  })
  //DIVIDER
  menuItems.push({
    // This item is a divider (shows only gray line, no text etc.)
    type: ContextMenu.DIVIDER
  })
  // DELETE NODE
  menuItems.push({
    text: `Delete ${node.data.label}`,
    icon: `<img class="cm-svg-icon" src="${BaseLineDelete}" alt="add organization"/>`,
    enabled: true,
    events: {
      click: e => {
        fn({
          type: actionType.DELETE_NODE,
          node
        })
      }
    }
  })

  return menuItems
}

function getLinkingRule(srcType, trgType) {
  //debugger
  const linkRules = cgc.getConfigurationSection('linkRules')
  const source = linkRules[srcType]
  let canLink = false
  if (source && source.hasOwnProperty(trgType) === true) {
    canLink = source[trgType]
  }
  return canLink
}

function getNodeSize(nodeType) {
  const nodeProps = cgc.getConfigurationSection('nodeProps')
  let nodeSize = {
    ...nodeProps.typeSize[nodeType]
  }
  return nodeSize
}

function getNodeProps(nodeType) {
  const nodeProps = cgc.getConfigurationSection('nodeProps')
  let props = {
    defaultProps: {
      ...nodeProps['defaultProps']
    },
    typeProps: {
      ...nodeProps.typeProps[nodeType]
    }
  }
  return props
}

function getNodeType(typeKey) {
  const nodeTypes = cgc.getConfigurationSection('nodeTypes')
  const nodeType = nodeTypes[typeKey]
  return nodeType
}

function getSvgMenuItems(node, fn) {
  //debugger
  const nodeTypes = cgc.getConfigurationSection('nodeTypes')
  let menuItems = []
  let keys = Object.keys(nodeTypes)

  keys.forEach(key => {
    let item = nodeTypes[key]
    let action = {
      type: actionType.ADD_NODE,
      payload: {
        nodeType: item,
        nodeProps: getNodeProps(key),
        nodeSize: getNodeSize(key),
        node
      }
    }
    let menuItem = {
      text: `Add ${item.label.toLowerCase()}`,
      icon: `<div class="svg-menu-type-icon" style="${'background-color:' +
        item.color}"></div>`,
      enabled: true,
      events: {
        click: e => {
          fn(action)
        }
      }
    }
    menuItems.push(menuItem)
  })
  // TO DO!
  // when linking is in progress
  // add cancel linking to svg context menu
  // debugger
  // if (node.data.state) {
  //   //DIVIDER
  //   menuItems.push({
  //     // This item is a divider (shows only gray line, no text etc.)
  //     type: ContextMenu.DIVIDER
  //   })
  //   menuItems.push({
  //     text: 'Cancel linking',
  //     icon: `<img class="cm-svg-icon" src="${BaseLineClear}" alt="add person"/>`,
  //     enabled: true,
  //     events: {
  //       click: e => {
  //         fn({
  //           type: actionType.CANCEL_LINK_PROCESS,
  //           node
  //         })
  //       }
  //     }
  //   })
  // }
  return menuItems
}

function getViewerMenuItems(node, fn) {
  return [
    {
      text: `${'Pin ' + node.data.label}`,
      icon: `<img class="cm-svg-icon" src="${BaseLinePinNode}" alt="toggle node"/>`,
      enabled: !node.data.fixed,
      events: {
        click: e => {
          fn({
            type: actionType.TOGGLE_PINNED_NODE,
            node
          })
        }
      }
    },
    {
      // This item is a divider (shows only gray line, no text etc.)
      type: ContextMenu.DIVIDER
    },
    {
      text: `${'Unpin ' + node.data.label}`,
      icon: `<img class="cm-svg-icon" src="${BaseLinePinNode}" alt="toggle node"/>`,
      enabled: node.data.fixed || false,
      events: {
        click: e => {
          fn({
            type: actionType.TOGGLE_PINNED_NODE,
            node
          })
        }
      }
    }
  ]
}

export function alowedTargetTypes(sourceType) {
  const linkRules = cgc.getConfigurationSection('linkRules')
  return linkRules[sourceType]
}

/**
 * Validates modal data based on the list of required fields defined
 * in the state and props loaded in the state. This function uses
 * additional information about property format (min,max) and stores
 * data in the value property.
 * @param {Object} data the data object to validate (state)
 * @param {Array} required list of required properties
 * @param {EventTarget} target event target from input element
 * @param {String} target.name name of the input element
 * @param {String} target.value current value in the input element
 * @returns {Boolean}
 */
export function isModalValid(data, required, { name, value }) {
  let valid = true
  // debugger
  for (let i in required) {
    let key = required[i]
    let val = ''
    let min = data[key].min
    let typ = data[key].type
    if (key === name) {
      val = value
    } else {
      val = data[key].value
    }
    if (val === '') {
      valid = false
      break
    }
    if (typ === actionType.TEXT && val.length < min) {
      valid = false
      break
    }
  }
  return valid
}
/**
 * Validates config form data based on the list of required fields defined
 * in the state
 * @param {Object} data the data object to validate (state)
 * @param {Array} required list of required properties
 * @param {EventTarget} target event target from input element
 * @param {String} target.name name of the input element
 * @param {String} target.value current value in the input element
 * @returns {Boolean}
 */
export function isConfigValid(data, required, { name, value }) {
  let valid = true
  for (let i in required) {
    let field = required[i]
    let val = ''
    if (field === name) {
      val = value
    } else {
      val = data[field]
    }
    if (val === '') {
      valid = false
      break
    }
  }
  return valid
}

/**
 * Create unique property Id based on label value
 * Simply remove spaces and some specific chars
 * @param {String} label text value from user
 */
export function constructPropertyId(label) {
  let id = label
    .replace(/\s+/g, '_')
    .replace(/\W/g, '_')
    .toUpperCase()
  return id
}

export default {
  getNodeContextMenu,
  alowedTargetTypes,
  isModalValid,
  isConfigValid,
  constructPropertyId
}
