/**
 * Configuration object for simulation definitions
 * used in d3.util.js and in configuration for defaults
 */

// import svcHasura from './Graphql/svcHasura'
import { CONSTANT_NODE_SIZE, TEXT, SLIDER } from './cg.types'

// local configuration object
// see cg.config.sample.jsonc
let configuration = {}

function setConfiguration(config) {
  configuration = config
}
function setConfigurationSection(section, data) {
  configuration[section] = data
}
function getConfiguration(section) {
  return configuration
}
function getConfigurationSection(section) {
  if (configuration.hasOwnProperty(section) === true) {
    return configuration[section]
  } else {
    return {}
  }
}

export function getDefaultNodeSize() {
  if (configuration.hasOwnProperty('simulation')) {
    let sim = configuration['simulation']
    if (sim.hasOwnProperty('defaultNodeSize')) {
      return sim.defaultNodeSize.value
    } else {
      //return default value
      return defaultSimCfg.defaultNodeSize.value
    }
  } else {
    //return default value
    return defaultSimCfg.defaultNodeSize.value
  }
}

/**
 * Used as default values
 * in configuration wizard
 * all default values are required
 * System values are created by app
 */
export const defaultNodeProps = {
  id: {
    id: 'id',
    label: 'Node id',
    type: TEXT,
    min: 5,
    max: 50,
    required: true,
    system: true
  },
  type: {
    id: 'type',
    label: 'Node type',
    type: TEXT,
    min: 1,
    max: 50,
    required: true,
    system: true
  },
  label: {
    id: 'label',
    label: 'Label',
    type: TEXT,
    min: 2,
    max: 3,
    required: true,
    system: false
  }
}
/**
 * Used as default values
 * in configuration wizard
 * value is defined in
 * defaultSimCfg.defaultNodeSize.value
 */
export const constantNodeSize = {
  type: CONSTANT_NODE_SIZE,
  label: 'Constant'
}
/**
 * This values are used by d3.scaleLinear
 * to scale node value based on custom
 * properties. See d3.util.calculateRadius()
 */
export const defaultScaleRange = {
  min: 400,
  max: 4000
}
/**
 * Default simulation configuration.
 * It includes prop info for the form.
 * System values (system:true) are not shown to user.
 */
export const defaultSimCfg = {
  strokeColor: {
    id: 'strokeColor',
    label: 'Line color',
    info:
      'The color used for links and node border. Use hex value (incl. #) or color name.',
    type: TEXT,
    min: 3,
    max: 20,
    required: true,
    system: false,
    value: '#999'
  },
  strokeWidth: {
    id: 'strokeWidth',
    label: 'Link width',
    info: 'The width of the connectors/lines in pixels.',
    type: SLIDER,
    min: 1,
    max: 10,
    step: 1,
    required: true,
    system: false,
    value: 3
  },
  defaultNodeSize: {
    id: 'defaultNodeSize',
    label: 'Default node size',
    info: 'Default node radius in pixels.',
    type: SLIDER,
    min: 20,
    max: 100,
    step: 1,
    required: true,
    system: false,
    value: 24
  },
  zoomMin: {
    id: 'zoomMin',
    label: 'Zoom out',
    // start value of simulation
    // when 1 force is strong
    info:
      'How far user can zoom out. Value 1 means no zoom out. Values < 1 enable zoom out up to defined value.',
    type: SLIDER,
    min: 0.1,
    max: 1,
    step: 0.1,
    required: true,
    system: false,
    value: 0.2
  },
  zoomMax: {
    id: 'zoomMax',
    label: 'Zoom in',
    // start value of simulation
    // when 1 force is strong
    info:
      'How far user can zoom in. Value 1 means no zoom in. Values > 1 enable scale up to defined value.',
    type: SLIDER,
    min: 1,
    max: 7,
    step: 0.5,
    required: true,
    system: false,
    value: 2
  },
  chargeStrength: {
    id: 'chargeStrength',
    label: 'Charge strength',
    info:
      'Negative value (charge) reppels and positive attracts the nodes. It works together with link distance and collide strength to calculate position of each node in the space during simulation run.',
    type: SLIDER,
    min: -200,
    max: 200,
    step: 1,
    required: true,
    system: false,
    value: -50
  },
  alphaValue: {
    id: 'alphaValue',
    label: 'Alpha value',
    // start value of simulation
    // when 1 force is strong
    info:
      'Simulation duration and strenght. Higer values produce longer simulation and "smoother" positoning of nodes.',
    type: SLIDER,
    min: 0.1,
    max: 1,
    step: 0.05,
    required: true,
    system: false,
    value: 0.5
  },
  collideStrength: {
    id: 'collideStrength',
    label: 'Collide strength',
    // > 1 does not allow nodes overlay
    info:
      'Higher value force larger distance between the nodes. Value > 1 does not allow node to be at the top of another node.',
    type: SLIDER,
    min: 0,
    max: 3,
    step: 0.1,
    required: true,
    system: false,
    value: 1.5
  },
  linkDistance: {
    id: 'linkDistance',
    label: 'Link distance strength',
    info:
      'The distance between nodes expressed as value. It works in conjuction with collide strength value to calculate the length of the connectors.',
    type: SLIDER,
    min: 1,
    max: 200,
    step: 1,
    required: true,
    system: false,
    value: 50
  },
  alphaMin: {
    id: 'alphaMin',
    label: 'Alpha min',
    info: 'When the simulation will stop.',
    type: SLIDER,
    min: 0.01,
    max: 0.1,
    step: 0.01,
    required: true,
    //hidden value
    system: true,
    value: 0.07
  },
  alphaTarget: {
    id: 'alphaTarget',
    label: 'Alpha target',
    info: `Target value of the simulation.
      It needs to be LOWER than alphaMin value for
      simulaton to complete.`,
    type: SLIDER,
    min: 0.01,
    max: 0.1,
    step: 0.01,
    required: true,
    //hidden value
    system: true,
    value: 0.05
  },
  svg: {
    id: 'svg',
    system: true,
    minWidth: 300,
    minHeight: 400,
    width: null,
    height: null
  }
}

export default {
  getConfiguration,
  getConfigurationSection,
  setConfiguration,
  setConfigurationSection
}
