import React, { Component } from 'react'

import * as actionType from './Helpers/cg.types'
import svcHasura from './Graphql/svcHasura'
import { extractHasuraError } from './Graphql/gqlHelpers'

import d3u from './Helpers/d3.util'
import cgu from './Helpers/cg.util'
import cgc from './Helpers/cg.config'
import ContextMenu from './ContextMenu/ContextMenu'
import Toolbox from './Toolbox/Toolbox'
import {
  NodeModal,
  LoaderModal,
  ErrorModal,
  InfoModal
} from './Modals'

import EmptyPlaceholder from './Placeholders/EmptyPlaceholder'
import Configuration from './Configuration/Configuration'
import SimSettings from './SimSettings/SimSettings'
import InfoPanel from './InfoPanel/InfoPanel'

import './ForceDirectedGraph.scss'
import AuthSvc from '../AuthRoute/AuthSvc'

class ForceGraph extends Component {
  // state is used to load different modal types
  // flags new setup (no configuration values)
  // and keep track of sourceNode for linking
  state = {
    showModal: true,
    newSetup: false,
    action: {
      type: actionType.SHOW_LOADER,
      payload: {
        title: 'Loading data...',
        type: 'packman'
      }
    },
    sourceNode: null,
    loaderStartTime: new Date().getTime(),
    loaderMinDisplayTime: 500
  }
  // class level variables are used
  // by D3 to persist 'state' of
  // force directed graph through React
  // updates and perform updates without
  // redrawing the component
  chartDiv = React.createRef()
  // D3 data reference
  data = null
  // popup menu reference
  nodeContextMenu = null
  // node nodes flag
  noNodes = true
  /**
   * Initialize D3 force directed graph
   * it is called once
   * @param {Object} data object with nodes and links
   * @param {Array} data.nodes array of node object
   * @param {Array} data.links array of link object
   */
  initForce(data) {
    try {
      let svgSize = d3u.getChartSize(
        document.querySelector('#d3-force-graph')
      )
      let { nodes: dataNodes, links: dataLinks } = data
      d3u.createSvg(this.chartDiv.current, svgSize, {
        contextmenu: this.showNodeContextMenu
      })
      let simCfg = cgc.getConfigurationSection('simulation')
      let config = {
        ...simCfg,
        svg: {
          ...simCfg.svg,
          ...svgSize
        }
      }
      d3u.initSimulation(config)
      let validLinks = d3u.validateDataLinks(
        dataLinks,
        dataNodes
      )
      this.data = {
        dataNodes,
        dataLinks: validLinks
      }
      this.renderForce(this.data, config)
    } catch (e) {
      console.error('ForceDirectedGraph.initForce...', e)
      throw new Error(e.message)
    }
  }
  /**
   * Renders existing force directed chart after editing
   * nodes or links
   * @param {Object} data object with links and nodes
   * @param {Array} data.nodes array of data nodes
   * @param {Array} data.links  array of data links
   * @param {Object} config  simulation configuration/parameters
   */
  renderForce(data, config = {}) {
    try {
      let { dataNodes, dataLinks } = data
      this.updateD3Links(dataLinks)
      this.updateD3Nodes(dataNodes)
      //check for empty
      if (dataNodes.length === 0) {
        //console.warn('Nothing to show here!')
        d3u.stopSimulation()
        this.noNodes = true
      } else {
        this.noNodes = false
        d3u.updateSimulation(config)
      }
    } catch (e) {
      console.error('Failed to render force chart...', e)
    }
  }
  updateD3Links(dataLinks) {
    d3u.updateLinks(dataLinks, {
      click: this.deleteLink,
      contextmenu: function(d) {
        console.warn(
          'contextmenu called on link but not handled...',
          d
        )
      }
    })
  }
  updateD3Nodes(dataNodes) {
    d3u.updateNodes(dataNodes, {
      click: this.clickedOnLinkTargetNode,
      contextmenu: this.showNodeContextMenu
    })
  }
  /**
   * Updates force directed graph with the 'latest' data from the server.
   * This method should be called on 'reset' because it (re)initializes
   * simulation.
   */
  reloadForce() {
    svcHasura
      .getNodesAndLinks()
      .then(res => {
        let validLinks = d3u.validateDataLinks(
          res.links,
          res.nodes
        )
        this.renderForce({
          dataNodes: res.nodes,
          dataLinks: validLinks
        })
        //reload local data
        this.data = {
          dataNodes: res.nodes,
          dataLinks: validLinks
        }
        this.hideLoader()
      })
      .catch(e => {
        console.error('Failed to update force chart:', e)
        this.hideLoader()
      })
  }
  /**
   * Shows the node context menu. Method is called from D3 node (d3.util)
   * on right-mouse-click (on contextmenu event). The menu items are
   * constructed for each node type. The callback function
   * (nodeContextMenuAction) is called from nodeContextMenu passing the
   * action and the payload.
   * @param {Object} node D3 node object with all node data avaliable incl. internal D3 vars
   * @param {MouseEvent} node.event D3 mouse event object
   * @param {SVGElement} node.item D3 object refference to clicked svg element
   * @param {Object} node.data node data loaded in D3
   */
  showNodeContextMenu = node => {
    //debugger
    let menuItems = cgu.getNodeContextMenu(
      node,
      this.nodeContextMenuAction
    )
    if (this.nodeContextMenu) {
      //update menu items if the instance is already present
      this.nodeContextMenu.update(menuItems)
    } else {
      //create ContextMenu instance if none present
      this.nodeContextMenu = new ContextMenu(menuItems)
    }
    //display context menu at the position of svg node
    this.nodeContextMenu.display(node.event, node.item)
  }
  /**
   * Context menu action selected by user.
   * Ations types which do not need component state update are intercepted here.
   * By default the method will update the state of the component and pass the
   * action type and payload
   * @param {Object} action object with action info
   * @param {String} action.type type of the action selected by user
   * @param {Object} action.node D3 object reference to svg element issuing action
   */
  nodeContextMenuAction = action => {
    switch (action.type) {
      case actionType.SHOW_LOADER:
        this.setState({
          showModal: true,
          action,
          sourceNode: action.node
        })
        break
      case actionType.ADD_NODE:
      case actionType.EDIT_NODE:
        this.setState({
          showModal: true,
          action,
          sourceNode: action.payload.node
        })
        break
      case actionType.ADD_NODE_AND_LINK:
        this.setState({
          showModal: true,
          action,
          sourceNode: action.payload.node
        })
        break
      case actionType.DELETE_NODE:
        this.deleteNodeAndItsLinks(action)
        break
      case actionType.TOGGLE_PINNED_NODE:
        d3u.togglePinnedNode(action.node)
        break
      case actionType.START_LINK_PROCESS:
        //console.log(`${action.type}...`, action)
        this.activateLinksToSourceNode(action)
        break
      case actionType.COMPLETE_LINK_PROCESS:
        this.clickedOnLinkTargetNode(action.node)
        break
      case actionType.CANCEL_LINK_PROCESS:
        this.cancelLinkProcess()
        break
      default:
        console.warn(
          `nodeContextMenuAction: actionType [${
            action.type
          }] not supported`
        )
    }
  }
  /**
   * Handles cancel/save action from the modals.
   * These user actions arrive here.
   * @param {Object} action user action from the modal
   * @param {String} action.type any of types defined in cg.type.js
   * @param {Object} action.payload node data from the modal
   */
  handleModalAction = action => {
    switch (action.type) {
      case actionType.CANCEL_ACTION:
        //close modal and remove sourceNode
        this.setState({
          showModal: false,
          action: null,
          sourceNode: null
        })
        break
      case actionType.ADD_NODE:
        //add non-linked node
        this.addNode(action.payload)
        break
      case actionType.ADD_NODE_AND_LINK:
        //add new node and link
        this.addNodeAndLinkToSourceNode(action)
        break
      case actionType.EDIT_NODE:
        if (action.payload.id) {
          this.updateNodeAfterEdit(action)
        } else {
          console.warn('Cannot update node...id is missing')
        }
        break
      case actionType.SAVE_CONFIGURATION:
        this.saveConfiguration(action)
        break
      case actionType.SIM_SETTINGS:
        this.updateSimulationConfiguration(action)
        break
      case actionType.CLOSE_MODAL:
        //close modal
        this.setState({
          showModal: false,
          action: null
        })
        break
      default:
        console.warn(
          `handleModalAction: action [${
            action.type
          }]...not supported`
        )
    }
  }
  addNodeToD3Data(newNode, gqlResponse) {
    let { id, dataB64 } = gqlResponse
    newNode['id'] = id
    newNode['dataB64'] = dataB64
    //delete plain data
    delete newNode.data
    //push new data
    this.data.dataNodes.push(newNode)
    //noNodes flag
    this.noNodes = false
  }
  addLinkToD3Data(newLink, gqlResponse) {
    //debugger
    let { id, dataB64 } = gqlResponse
    newLink['id'] = id
    newLink['dataB64'] = dataB64
    //delete plain data
    delete newLink.data
    //push new data
    this.data.dataLinks.push(newLink)
  }
  /**
   * Add new node to database and D3
   */
  addNode = newNodeData => {
    // add node to database
    svcHasura
      .addNode(newNodeData)
      .then(res => {
        //add node to D3 data object
        this.addNodeToD3Data(newNodeData, res)
        //update visualization
        this.updateD3Nodes(this.data.dataNodes)
        //it updates nodes and links
        d3u.updateSimulationNodesAndLinks()
        //restart to place node on canvas
        d3u.restartSimulation(0.3)
        this.removeSourceNodeFromState()
        this.hideLoader()
      })
      .catch(e => {
        console.error('Failed to add new node ...', e)
        this.removeSourceNodeFromState()
        this.hideLoader()
      })
    //show loader
    this.showLoader({
      title: `Adding ${newNodeData.label}...`,
      type: actionType.LOADER_BALL_TRIANGLE,
      minWatingTime: 500
    })
  }
  /**
   * Add new node to database and create new link
   * between new node and 'sourceNode' -> node that
   * issued the request
   */
  addNodeAndLinkToSourceNode = action => {
    //debugger
    const { sourceNode, payload: newNodeData } = action
    //set inital position of new node
    // to be equal to sourceNode
    if (sourceNode.data.x) {
      newNodeData['x'] = sourceNode.data.x
      newNodeData['y'] = sourceNode.data.y
    }
    let newLink = {}
    // add node to database
    svcHasura
      .addNode(newNodeData)
      .then(res => {
        //add node to D3 data object
        this.addNodeToD3Data(newNodeData, res)
        //now add link to existsing node
        //using sourceNode id and newNode id
        //received from database
        newLink = {
          source: sourceNode.data.id,
          target: res.id,
          data: {
            stid: `${sourceNode.data.id}_${res.id}`
          }
        }
        return svcHasura.addLink(newLink)
      })
      .then(res => {
        //add link to D3
        this.addLinkToD3Data(newLink, res)
        //update visualization
        this.updateD3Nodes(this.data.dataNodes)
        this.updateD3Links(this.data.dataLinks)
        // it updates nodes and links
        d3u.updateSimulationNodesAndLinks()
        //debugger
        d3u.restartSimulation(0.2)
        this.removeSourceNodeFromState()
        this.hideLoader()
      })
      .catch(e => {
        console.error(
          'Failed to add node and link to source node...',
          e
        )
        this.removeSourceNodeFromState()
        this.hideLoader()
      })
    //show loader
    this.showLoader({
      title: `Adding ${newNodeData.label}...`,
      type: actionType.LOADER_BALL_TRIANGLE,
      minWatingTime: 500
    })
  }
  /**
   * Updates existing node in the database after its been
   * edited in the modal. The user action is handled by
   * handleModalAction method and in case of edit the action
   * and payload are send to this method.
   * @param {Object} action
   * @param {Object} action.payload the data to be send to database
   * @param {String} action.payload.id id used to update node in database
   * @param {String} action.payload.type type to update
   * @param {String} action.payload.label label to update
   * @param {Object} action.payload.data other data for the update.
   */
  updateNodeAfterEdit = action => {
    const { payload, sourceNode } = action
    svcHasura
      .updateNodeById(payload)
      .then(res => {
        if (res.affected_rows > 0) {
          //add received data from backend
          let newData = {
            ...payload,
            dataB64: res.dataB64
          }
          //remove raw data received from modal
          delete newData.data
          //update node in D3
          this.updateNodeInD3Data(sourceNode, newData)
        } else {
          throw Error(
            `Failed to update node. Node with id [${
              payload.id
            }] not found.`
          )
        }
        this.hideLoader()
      })
      .catch(e => {
        console.error('Failed to update node. ', e)
        this.hideLoader()
      })
    this.showLoader({
      title: `Updating ${payload.label}...`,
      type: actionType.LOADER_BALL_TRIANGLE,
      minWatingTime: 500
    })
  }
  /**
   * Updates existing node in the data. It
   * needs to keep refference to old node in order
   * to let D3 know that this is existing data node.
   * @param {*} svgNode
   * @param {*} newData
   */
  updateNodeInD3Data(svgNode, newData) {
    //get node index
    const { index } = svgNode.data
    if (this.data.dataNodes[index]) {
      let keys = Object.keys(newData)
      keys.forEach(key => {
        let newVal = newData[key]
        this.data.dataNodes[index][key] = newVal
      })
      d3u.updateNodes(this.data.dataNodes)
    } else {
      console.warn(
        `updateNodeInD3Data...dataNodes[${index}]...missing`
      )
      this.reloadForce()
    }
  }
  /**
   * It marks visualy nodes that are avaliable for
   * linking based on sourceNode that issued request.
   */
  activateLinksToSourceNode(action) {
    const { type, id } = action.node.data
    const validTypes = cgu.alowedTargetTypes(type)
    let validCount = 0

    // add other nodes
    let alreadyConnected = []
    // add sourceNode
    alreadyConnected.push(id)
    // check all links
    this.data.dataLinks.forEach(l => {
      if (l.source.id === id) {
        alreadyConnected.push(l.target.id)
      } else if (l.target.id === id) {
        alreadyConnected.push(l.source.id)
      }
    })
    // loop nodes
    this.data.dataNodes.forEach(n => {
      if (n.id === id) {
        n.state = actionType.SOURCE_NODE
      } else if (
        validTypes[n.type] === true &&
        alreadyConnected.indexOf(n.id) === -1
      ) {
        n.state = actionType.TARGET_NODE
        validCount += 1
      } else {
        n.state = actionType.INVALID_NODE
      }
    })
    if (validCount === 0) {
      const { label = ' this node ' } = action.node.data
      this.setState({
        showModal: true,
        action: {
          type: actionType.SHOW_INFO_MODAL,
          payload: {
            title: `Link ${action.node.data.label}`,
            message: `
              There are no nodes that can be linked with ${label}.
              Either are all nodes valid for linking with ${label}
              already linked to ${label} or there are no "nodes"
              that are valid for linking with ${label}.
            `
          }
        }
      })
      //clean state from nodes
      this.removeStatePropFromD3Nodes()
    } else {
      // update nodes on canvas
      d3u.updateNodes(this.data.dataNodes)
      // change links opacity
      d3u.setLinksOpacity(0.3)
      //save sourceNode in the sate
      this.setState({
        showModal: false,
        action: action,
        sourceNode: action.node
      })
    }
  }
  /**
   * Method is called when user have clicked on the node
   * avaliable for linking with sourceNode (a.k.a targetNode).
   * It is also called by contextmenu item Complete linking.
   * @param {Object} node D3 node incl. svg element and click event
   */
  clickedOnLinkTargetNode = node => {
    const { data: targetNodeData } = node
    const { data: sourceNodeData } = this.state.sourceNode
    if (!sourceNodeData || !targetNodeData) {
      console.warn('Cannot link nodes...node data missing')
      return false
    }
    //add new link
    this.addLinkBetweenNodes(sourceNodeData, targetNodeData)
  }
  /**
   * Add new link to database and to D3 data.
   * It returnes promise with newLink object.
   * @param {Object} sourceNode D3 data node
   * @param {Object} targetNode D3 data node
   * @returns {Promise<newLink>} newLink D3 dataLink object
   */
  addLinkBetweenNodes = (sourceNode, targetNode) => {
    let newLink = {
      source: sourceNode.id,
      target: targetNode.id,
      data: {
        stid: `${sourceNode.id}_${targetNode.id}`
      },
      x1: sourceNode.x,
      y1: sourceNode.y,
      x2: targetNode.x,
      y2: targetNode.y
    }
    this.toggleWaitCursor(true)
    //add link to database
    svcHasura
      .addLink(newLink)
      .then(res => {
        return this.addLinkToD3Data(newLink, res)
      })
      .then(() => {
        this.removeStatePropFromD3Nodes()
        this.updateD3Links(this.data.dataLinks)
        this.updateD3Nodes(this.data.dataNodes)
        d3u.updateSimulationNodesAndLinks()
        d3u.restartSimulation(0.2)
        d3u.setLinksOpacity(1)
        this.toggleWaitCursor(false)
      })
      .catch(e => {
        console.error(e)
        this.toggleWaitCursor(false)
      })
  }
  cancelLinkProcess() {
    this.removeStatePropFromD3Nodes()
    this.updateD3Nodes(this.data.dataNodes)
  }
  /**
   * Remove state property on all D3 dataNodes
   * State is used during linking process to
   * indicate source node, target node and
   * invalid node status for current linking.
   * After linking process is completed or
   * cancelled the state prop need to be removed.
   */
  removeStatePropFromD3Nodes() {
    this.data.dataNodes.forEach(n => {
      delete n.state
    })
  }
  /**
   * Delete link in database and in D3 canvas.
   * @param {Object} link D3 svg element and the dataLink
   * @param {SVGElement} link.item D3 svg element refference
   * @param {Object} link.data linkData object of link item
   */
  deleteLink = link => {
    const { id } = link.data
    this.toggleWaitCursor(true)
    //delete link in database
    svcHasura
      .deleteLinkById(id)
      .then(res => {
        //debugger
        if (res.affected_rows === 1) {
          //delete link in D3 data
          this.deleteLinkFromD3Data(id)
        } else {
          throw Error('Failed to remove link in database')
        }
        this.updateD3Links(this.data.dataLinks)
        d3u.updateSimulationNodesAndLinks()
        //restart disabled on 2019-03-30
        //to avoid repositioning of nodes
        //d3u.restartSimulation(0.2)
        this.toggleWaitCursor(false)
        //we need to redraw to
        //update info panel legend with
        //proper node and link counts
        this.setState({
          action: {
            type: actionType.NO_ACTION
          }
        })
      })
      .catch(e => {
        console.error('Delete link failed: ', e)
        this.updateD3Links(this.data.dataLinks)
        d3u.updateSimulationNodesAndLinks()
        d3u.restartSimulation()
        this.toggleWaitCursor(false)
      })
  }
  deleteLinkFromD3Data(id) {
    try {
      let links = this.data.dataLinks.filter(l => {
        return l.id !== id
      })
      this.data.dataLinks = links
    } catch (e) {
      console.error('Delete link failed: ', e)
    }
  }
  /**
   * Deletes current node from the database
   * including all links from other nodes to this node.
   * Then it calls the method to delete node and its links
   * from D3 simulation data.
   */
  deleteNodeAndItsLinks(action) {
    let { id } = action.node.data
    if (id) {
      //console.log('Delete node...', id)
      svcHasura
        .deleteNodeAndLinksByNodeId(id)
        .then(res => {
          this.deleteCurrentSourceNodeFromD3(id)
        })
        .catch(e => {
          console.error('Failed to delete node...', e)
        })
    } else {
      console.error(
        'Cannot remove current sourceNode...id missing'
      )
    }
  }
  /**
   * Deletes current sourceNode from D3 simulation
   * and removes all related links.
   */
  deleteCurrentSourceNodeFromD3(id) {
    // this.data.dataNodes.splice(index, 1)
    let nodes = this.data.dataNodes.filter(n => {
      return n.id !== id
    })
    //delete related links
    let links = this.data.dataLinks.filter(l => {
      return l.source.id !== id && l.target.id !== id
    })
    this.data.dataNodes = nodes
    this.data.dataLinks = links
    //update nodes
    this.updateD3Nodes(this.data.dataNodes)
    //update links
    this.updateD3Links(this.data.dataLinks)
    //update simulation data
    d3u.updateSimulationNodesAndLinks()
    //restart disabled on 2019-03-30
    //to avoid repositioning of nodes
    //d3u.restartSimulation(0.1)
    //check for empty nodes
    if (nodes.length === 0) {
      this.noNodes = true
      d3u.stopSimulation()
    } else {
      this.noNodes = false
    }
    //we need to redraw to
    //update info panel legend with
    //proper node and link counts
    this.setState({
      action: {
        type: actionType.NO_ACTION
      }
    })
  }
  removeSourceNodeFromState() {
    this.setState({
      sourceNode: null
    })
  }
  showLoader({
    minWatingTime = 1000,
    title = 'Loading...',
    type = actionType.LOADER_BALL_TRIANGLE
  }) {
    this.setState({
      showModal: true,
      action: {
        type: actionType.SHOW_LOADER,
        payload: {
          title,
          type
        }
      },
      loaderStartTime: new Date().getTime(),
      loaderMinDisplayTime: minWatingTime
    })
  }
  hideLoader() {
    // set timer to wait minimum amount of milliseconds
    // and avoid almost immediate remove of the loader
    let start = this.state.loaderStartTime
    let min = this.state.loaderMinDisplayTime
    let now = new Date().getTime()
    let wait = start + min

    const setLoaderState = () => {
      this.setState({
        showModal: false,
        newSetup: false,
        action: null,
        loaderStartTime: null
      })
    }

    if (wait > now) {
      let timeout = wait - now
      setTimeout(() => {
        setLoaderState()
      }, timeout)
    } else {
      setLoaderState()
    }
  }
  toggleWaitCursor(show = true) {
    if (show) {
      this.chartDiv.current.style.cursor = 'wait'
    } else {
      this.chartDiv.current.style.cursor = 'default'
    }
  }
  /**
   * Renders different modals based on state updates.
   * NOTE! the svg should not be re-rendered by React.
   */
  renderActionHtml = () => {
    let dynamicHtml = null
    if (this.state.showModal === true) {
      const { action } = this.state
      switch (action.type) {
        case actionType.ADD_NODE:
        case actionType.EDIT_NODE:
        case actionType.ADD_NODE_AND_LINK:
          dynamicHtml = (
            <NodeModal
              action={action}
              onModalAction={this.handleModalAction}
            />
          )
          break
        case actionType.APP_SETTINGS:
          dynamicHtml = (
            <Configuration
              newSetup={this.state.newSetup}
              config={cgc.getConfiguration()}
              onModalAction={this.handleModalAction}
            />
          )
          break
        case actionType.SHOW_LOADER:
          dynamicHtml = (
            <LoaderModal
              title={action.payload.title}
              type={action.payload.type}
            />
          )
          break
        case actionType.FATAL_ERROR:
          dynamicHtml = (
            <ErrorModal
              title={action.payload.title}
              message={action.payload.message}
            />
          )
          break
        case actionType.SHOW_INFO_MODAL:
          dynamicHtml = (
            <InfoModal
              title={action.payload.title}
              message={action.payload.message}
              onModalAction={this.handleModalAction}
            />
          )
          break
        case actionType.NO_ACTION:
          return null
        default:
          console.warn(
            `renderActionHtml: actionType [${
              action.type
            }] not supported`
          )
      }
    }
    return dynamicHtml
  }
  onToolboxButton = action => {
    switch (action.type) {
      case actionType.RESTART_SIM:
        d3u.restartSimulation()
        break
      case actionType.RELOAD_SIM:
        this.reloadForce()
        break
      case actionType.RESET_ZOOM:
        d3u.resetZoom()
        break
      case actionType.URL_REDIRECT:
        this.props.onRedirect(action)
        break
      case actionType.APP_SETTINGS:
        this.setState({
          showModal: true,
          newSetup: false,
          action
        })
        break
      case actionType.SIM_SETTINGS:
        this.setState({
          showModal: false,
          newSetup: false,
          action
        })
        break
      case actionType.CLOSE_MODAL:
        this.setState({
          showModal: false,
          action
        })
        break
      case actionType.LOG_OUT:
        AuthSvc.logOut()
        //redirect to root
        this.props.onRedirect({
          type: actionType.URL_REDIRECT,
          payload: '/'
        })
        break
      default:
        console.warn(
          `onToolboxButton...action [${
            action.type
          }] not supported`
        )
    }
  }
  renderToolboxHtml = () => {
    // debugger
    let toolboxHtml = null
    if (this.props.toolbox) {
      let { title, buttons } = this.props.toolbox
      toolboxHtml = (
        <Toolbox
          title={title}
          buttons={buttons}
          onButtonClick={this.onToolboxButton}
        />
      )
    }
    return toolboxHtml
  }
  /**
   * Shows modal with error message
   * without option to close the modal.
   * Use this when app cannot be recovered.
   * The user will need to reload the page.
   */
  showFatalError({
    title = '500 Failed to connect',
    message = ''
  }) {
    this.setState({
      showModal: true,
      action: {
        type: actionType.FATAL_ERROR,
        payload: {
          title,
          message
        }
      }
    })
  }
  getPanelInfoSection = () => {
    let nodes = 0,
      links = 0
    if (this.data) {
      const { dataNodes, dataLinks } = this.data
      if (dataNodes) nodes = dataNodes.length
      if (dataLinks) links = dataLinks.length
    }
    let inHtml = (
      <p>
        The network has <b>{nodes}</b> nodes and <b>{links}</b>
        &nbsp;connections.
      </p>
    )
    return inHtml
  }
  getPanelLegendSection = () => {
    let inHtml = []
    let count = {}
    if (this.data) {
      const { dataNodes } = this.data
      const nodeTypes = cgc.getConfigurationSection('nodeTypes')
      //perform counts
      dataNodes.forEach(node => {
        if (count.hasOwnProperty(node.type) === false) {
          count[node.type] = 1
        } else {
          count[node.type] += 1
        }
      })
      //create legend
      let keys = Object.keys(nodeTypes)
      keys.forEach(key => {
        let type = nodeTypes[key]
        inHtml.push(
          <div className="legend-item" key={key}>
            <div
              className="NodeType-color-sample"
              style={{ backgroundColor: type.color }}
              title="Color sample"
            />
            <div className="legend-label">{type.label}</div>
            <div className="legend-count">
              {count[key] || 0} nodes
            </div>
          </div>
        )
      })
      return inHtml
    } else {
      return null
    }
  }
  renderGraphAreaHtml = () => {
    const { showModal, action } = this.state
    const { noNodes } = this
    let grapAreaHtml = []

    if (action && action.type === actionType.SIM_SETTINGS) {
      //get simulation settings
      action.payload = cgc.getConfigurationSection('simulation')
      //create settings window
      grapAreaHtml.push(
        <SimSettings
          key="sim-settings"
          action={action}
          onModalAction={this.handleModalAction}
        />
      )
    } else if (noNodes && !showModal) {
      grapAreaHtml.push(
        <EmptyPlaceholder
          key="empty-placeholder"
          className="no-nodes"
          title="No network to display"
          message="You need to be admin to create new network."
        />
      )
    } else {
      grapAreaHtml.push(
        <InfoPanel key="info-panel">
          <h1>Legend</h1>
          {this.getPanelLegendSection()}
          <p />
          <h1>Info</h1>
          {this.getPanelInfoSection()}
        </InfoPanel>
      )
    }
    if (grapAreaHtml.length === 0) {
      return null
    } else {
      return grapAreaHtml
    }
  }
  render() {
    // console.log('ForceDirectedGraph...state...', this.state)
    return (
      <React.Fragment>
        {this.renderToolboxHtml()}
        <section className="App-graph-area">
          <div id="d3-force-graph" ref={this.chartDiv} />
          {this.renderGraphAreaHtml()}
        </section>
        {this.renderActionHtml()}
      </React.Fragment>
    )
  }
  componentDidMount() {
    this.getConfiguration()
  }
  getConfiguration() {
    svcHasura
      .getConfiguration()
      .then(res => {
        if (res.errors) {
          this.showFatalError({
            title: '500 Failed to connect',
            message: extractHasuraError(res)
          })
        } else {
          const { nodeTypes } = res
          if (
            typeof nodeTypes === 'undefined' ||
            Object.keys(nodeTypes).length === 0
          ) {
            //nodeTypes does not exists or
            //does not have any key,
            this.handleNewSetup()
          } else {
            //save configuration for D3
            cgc.setConfiguration(res)
            //load nodes and links
            this.getNodesAndLinks()
          }
        }
      })
      .catch(e => {
        if (e.status === 401) {
          // console.warn('401 Unauthorized')
          this.showFatalError({
            title: '401 Not authorized',
            message:
              'Authentication hook unauthorized this request'
          })
        } else {
          this.showFatalError({
            title: '500 Failed to connect',
            message: `${e.message}`
          })
        }
      })
  }
  handleNewSetup() {
    let { onRedirect } = this.props
    AuthSvc.asyncTokenContainsUserRole(actionType.ROLE_ADMIN)
      .then(admin => {
        if (admin) {
          //this is new setup
          this.setState({
            showModal: true,
            newSetup: true,
            action: {
              type: actionType.APP_SETTINGS
            }
          })
        } else {
          //forward to admin route
          //for authentication
          //only admins can setup app
          onRedirect({
            action: actionType.URL_REDIRECT,
            payload: '/admin'
          })
        }
      })
      .catch(e => {
        this.showFatalError({
          title: '500 Failed to authenticate',
          message: e.message
        })
      })
  }
  getNodesAndLinks() {
    svcHasura
      .getNodesAndLinks()
      .then(res => {
        //intialize force chart
        this.initForce(res)
        //debugger
        this.hideLoader()
      })
      .catch(e => {
        this.showFatalError({
          title: '500 Failed to connect',
          message: `${e.message}`
        })
      })
  }
  /**
   * Save configuration to Hasura database and in local config object.
   * @params {String} action.type defined action types in cg.types.js
   * @params {Object} action.payload.nodeTypes configured node types
   * @params {Object} action.payload.nodeProps configured node props
   * @params {Object} action.payload.linkRules configured linking rules
   */
  saveConfiguration = action => {
    const { newSetup } = this.state
    this.showLoader({
      title: `Saving configuration...`,
      type: actionType.LOADER_BALL_TRIANGLE,
      minWatingTime: 500
    })
    // debugger
    // console.log(JSON.stringify(action.payload))
    if (newSetup) {
      // add new records to configuration table
      this.addConfiguration(action.payload)
    } else {
      // update existing records in configuration table
      this.updateConfiguration(action.payload)
    }
  }
  addConfiguration(config) {
    svcHasura
      .addConfiguration(config)
      .then(res => {
        // console.log('addConfig...', res)
        this.handleConfigurationResponse(config, res)
      })
      .catch(e => {
        this.handleConfigurationResponse(config, {
          errors: [e]
        })
      })
  }
  updateConfiguration(config) {
    svcHasura
      .updateConfiguration(config)
      .then(res => {
        // console.log('updateConfig...', res)
        this.handleConfigurationResponse(config, res)
      })
      .catch(e => {
        this.handleConfigurationResponse(config, {
          errors: [e]
        })
      })
  }
  handleConfigurationResponse(config, res) {
    if (res.errors) {
      this.showFatalError({
        title: '500 Failed to save',
        message: extractHasuraError(res)
      })
    } else {
      // svcHasura.saveConfiguration()
      // save configuration for D3 use
      cgc.setConfiguration(config)
      //no go and load everything from the start
      this.getNodesAndLinks()
    }
  }
  updateSimulationConfiguration(action) {
    svcHasura
      .updateConfigurationItem('simulation', action.payload)
      .then(res => {
        if (res === 1) {
          cgc.setConfigurationSection(
            'simulation',
            action.payload
          )
          //clear all elements
          d3u.clearAll()
          //reload everything
          this.reloadForce()
        } else {
          console.warn(`Updated...${res} records`)
        }
      })
      .catch(e => {
        console.error('Failed to update configuration...', e)
      })
    this.showLoader({
      title: `Updating simulation...`,
      type: actionType.LOADER_BALL_TRIANGLE,
      minWatingTime: 500
    })
  }
}

export default ForceGraph
