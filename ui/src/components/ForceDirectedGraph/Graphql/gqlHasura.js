import { createId, createdAt, encodeData } from './gqlHelpers'

let hasuraUrl = '/v1alpha1/graphql'
if (process.env.REACT_APP_API) {
  //console.log('hasura api...', process.env.REACT_APP_API)
  hasuraUrl = process.env.REACT_APP_API
}
/**
 * Hasura GraphQL queries
 * based on queries construced with GraphiQL in Hasura
 * local version http://localhost:8080/console/api-explorer
 * for use with fetch POST key query need to be added
 */
export const gqlHasura = {
  url: hasuraUrl,
  getConfuguration: () => {
    return {
      query: `{
        configuration{
          type
          dataB64
        }
      }`
    }
  },
  addConfiguration: config => {
    return {
      query: `mutation{
        insert_configuration(objects:[{
          type:"nodeTypes",
          dataB64:"${encodeData(config.nodeTypes)}"
        },{
          type:"nodeProps",
          dataB64:"${encodeData(config.nodeProps)}"
        },{
          type:"linkRules",
          dataB64:"${encodeData(config.linkRules)}"
        },{
          type:"simulation",
          dataB64:"${encodeData(config.simulation)}"
        }]){
          affected_rows
        }
      }`
    }
  },
  updateConfiguration: (type, data) => {
    return {
      query: `mutation{
        update_configuration(
          _set:{
            type:"${type}",
            dataB64:"${encodeData(data)}"
          }
          where:{type:{_eq:"${type}"}}
        ){
          affected_rows
        }
      }
      `
    }
  },
  getLinksAndNodes: () => {
    return {
      query: `{
        nodes{
          id
          type
          label
          dataB64
        }
        links{
          id
          source
          target
          dataB64
        }
      }`
    }
  },
  addNode: node => {
    return {
      query: `mutation{
          insert_nodes(objects:[{
            id:"${createId()}"
            createdAt:"${createdAt()}"
            label:"${node.label}"
            type:"${node.type}"
            dataB64:"${encodeData(node.data)}"}])
          {
            returning{
              id
              dataB64
            }
          }
        }
        `
    }
  },
  updateNodeById: node => {
    return {
      query: `
        mutation{
          update_nodes(
            _set:{
              label:"${node.label}"
              type:"${node.type}"
              dataB64:"${encodeData(node.data)}"
            }
            where:{id:{_eq:"${node.id}"}}){
            affected_rows
            returning{
              dataB64
            }
          }
        }
      `
    }
  },
  deleteOnlyNodeById: id => {
    return {
      query: `
        mutation{
          delete_nodes(
            where:{
              id:{_eq:"${id}"}
            }){
              affected_rows
            }
        }
        `
    }
  },
  deleteNodeAndLinksByNodeId: id => {
    return {
      query: `mutation{
        delete_nodes(
          where:{
            id:{_eq:"${id}"}
          }){
          affected_rows
        }
        delete_links(
          where:{
            _or:[
              {source:{_eq:"${id}"}}
              {target:{_eq:"${id}"}}
            ]
          }){
            affected_rows
          }
      }`
    }
  },
  addLink: link => {
    return {
      query: `
      mutation{
        insert_links(objects:[{
          id:"${createId()}"
          createdAt:"${createdAt()}"
          source:"${link.source}"
          target:"${link.target}"
          dataB64:"${encodeData(link.data)}"}]){
          returning{
            id
            dataB64
          }
        }
      }
      `
    }
  },
  deleteLinkById: id => {
    return {
      query: `
      mutation{
        delete_links(
          where:{id:{_eq:"${id}"}}
        ){
          affected_rows
        }
      }
      `
    }
  },
  deleteLinksFromNode: node_id => {
    return {
      query: `
        mutation{
          delete_links(
            where:{
            _or:[
              {source:{_eq:"${node_id}"}}
              {target:{_eq:"${node_id}"}}
            ]
          }){
            affected_rows
          }
        }
        `
    }
  }
}

export default gqlHasura
