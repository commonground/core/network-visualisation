/**
 * Create ID based on time in milliseconds and random number
 * @returns {String} unique id
 */
function createId() {
  const time = new Date().valueOf()
  const rand = Math.round(Math.random() * 1000000)
  return `${time}CG${rand}`
}

/**
 * CreatedAt date
 * @returns {String} date in ISO format
 */
function createdAt() {
  const d = new Date().toISOString()
  return d
}

/**
 * Encode node data to base64 string
 * Different node types have different
 * data properties
 * @param {Object} data node data object
 */
function encodeData(data) {
  let enc = btoa(JSON.stringify(data))
  return enc
}

/**
 * Decode node data to base64 string
 * Different node types have different
 * data properties
 * @param {Object} data node data object
 * @param {Object} defaultValue to return in case of error
 */
function decodeData(data, defaultValue) {
  let dec = null
  try {
    if (data) {
      dec = JSON.parse(atob(data))
      return dec
    } else {
      return defaultValue
    }
  } catch (e) {
    console.error(e)
    return defaultValue
  }
}

function extractHasuraError(res) {
  // debugger
  const { errors } = res
  let msg = 'Unknown server error'
  try {
    //we take message from first error object
    msg = errors[0].message
  } catch (e) {
    console.error('extractHasuraError...', e)
  }
  return msg
}

export {
  createId,
  createdAt,
  encodeData,
  decodeData,
  extractHasuraError
}
