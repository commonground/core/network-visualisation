/**
 * GraphQL queries
 */
import gql from './gqlHasura'
import { decodeData } from './gqlHelpers'
import authSvc from '../../AuthRoute/AuthSvc'
/**
 * Execute GraphQL query
 * @param {Object} gqlQuery valid GraphQL query object
 * @param {String} gqlQuery.query  valid GraphQL query string (use GraphiQL)
 * @returns {Object} data
 */
function execGraphQLQuery(gqlQuery) {
  let headers = authSvc.getHasuraRequestHeaders()
  return fetch(gql.url, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(gqlQuery)
  })
    .then(resp => {
      if (resp.status === 401) {
        throw resp
      } else {
        return resp.json()
      }
    })
    .then(({ data, errors }) => {
      if (errors) {
        return { errors }
      } else {
        return data
      }
    })
    .catch(err => {
      if (err.status === 401) {
        throw err
      } else {
        throw Error(`${err} ${gql.url}`)
      }
    })
}
/**
 * Decode configuration objects. All data is
 * stored in dataB64 text field as base64 stringified JSON
 * @param {Object} config.nodeTypes defined types of nodes
 * @param {Object} config.nodeProps defined props of each node type
 * @param {Object} config.linkRules linking rules between node types
 */
function decodeConfig(config) {
  let cfg = {}
  config.forEach(item => {
    let type = item['type']
    cfg[type] = decodeData(item['dataB64'], {})
  })
  return cfg
}

export const graphqlService = {
  /**
   * Returns nodes and links data arrays
   * @returns {Array} data.nodes
   * @returns {Array} data.links
   */
  getConfiguration: () => {
    return execGraphQLQuery(gql.getConfuguration()).then(
      res => {
        if (res.errors) {
          return res
        } else {
          const { configuration } = res
          let config = decodeConfig(configuration)
          return config
        }
      }
    )
  },
  /**
   * Adds new configuration records to configuration table
   * @params {Object} config.nodeTypes configured node types
   * @params {Object} config.nodeProps configured node props
   * @params {Object} config.linkRules configured linking rules
   */
  addConfiguration: config => {
    return execGraphQLQuery(gql.addConfiguration(config)).then(
      res => {
        if (res.errors) {
          return res
        } else {
          const {
            insert_configuration: { affected_rows }
          } = res
          return affected_rows
        }
      }
    )
  },
  /**
   * Updates all 4 records in the configuration table
   * @params {Object} config.nodeTypes configured node types
   * @params {Object} config.nodeProps configured node props
   * @params {Object} config.linkRules configured linking rules
   * @params {Object} config.simulation
   */
  updateConfiguration: config => {
    const {
      nodeTypes,
      nodeProps,
      linkRules,
      simulation
    } = config
    let affected_rows = 0
    return new Promise((resolve, reject) => {
      //update nodeTypes
      execGraphQLQuery(
        gql.updateConfiguration('nodeTypes', nodeTypes)
      )
        .then(res => {
          if (res.errors) {
            reject(res.errors)
          } else {
            affected_rows =
              res.update_configuration.affected_rows
            if (res.update_configuration.affected_rows === 0) {
              console.warn(
                'Failed to update configuration...nodeTypes'
              )
            }
            //update nodeProps
            return execGraphQLQuery(
              gql.updateConfiguration('nodeProps', nodeProps)
            )
          }
        })
        .then(res => {
          if (res.errors) {
            reject(res.errors)
          } else {
            affected_rows +=
              res.update_configuration.affected_rows
            if (res.update_configuration.affected_rows === 0) {
              console.warn(
                'Failed to update configuration...nodeProps'
              )
            }
            //update linkRules
            return execGraphQLQuery(
              gql.updateConfiguration('linkRules', linkRules)
            )
          }
        })
        .then(res => {
          if (res.errors) {
            reject(res.errors)
          } else {
            affected_rows +=
              res.update_configuration.affected_rows
            if (res.update_configuration.affected_rows === 0) {
              console.warn(
                'Failed to update configuration...linkRules'
              )
            }
            //update simulation
            return execGraphQLQuery(
              gql.updateConfiguration('simulation', simulation)
            )
          }
        })
        .then(res => {
          if (res.errors) {
            reject(res.errors)
          } else {
            affected_rows +=
              res.update_configuration.affected_rows
            if (res.update_configuration.affected_rows === 0) {
              console.warn(
                'Failed to update configuration...simulation'
              )
            }
            resolve({ affected_rows })
          }
        })
        .catch(e => {
          reject(e)
        })
    })
  },
  /**
   * Updates configuration item in configuration table
   * @params {String} type one of nodeType, propsType, linkRules, simulation
   * @params {Object} payload configuration item data
   * @returns {Promise} number of afected rows
   */
  updateConfigurationItem: (type, payload) => {
    let affected_rows = 0
    return new Promise((resolve, reject) => {
      //update nodeTypes
      execGraphQLQuery(gql.updateConfiguration(type, payload))
        .then(res => {
          if (res.errors) {
            reject(res.errors)
          } else {
            affected_rows =
              res.update_configuration.affected_rows
            if (res.update_configuration.affected_rows === 0) {
              console.warn(
                'Failed to update configuration...nodeTypes'
              )
            }
            resolve(affected_rows)
          }
        })
        .catch(e => {
          reject(e)
        })
    })
  },
  /**
   * Returns nodes and links data arrays
   * @returns {Array} data.nodes
   * @returns {Array} data.links
   */
  getNodesAndLinks: () => {
    return execGraphQLQuery(gql.getLinksAndNodes())
  },
  /**
   * Add node to collection of nodes
   * @param {Object} node
   * @param {String} node.type node type person,product,organisation
   * @param {String} node.label short label
   * @param {String} node.name node name
   * @param {Object} node.data all other data
   * @returns {String} node id
   */
  addNode: node => {
    return execGraphQLQuery(gql.addNode(node)).then(res => {
      if (res.errors) {
        return res
      } else {
        let {
          insert_nodes: {
            returning: {
              0: { id, dataB64 }
            }
          }
        } = res
        return {
          id,
          dataB64
        }
      }
    })
  },
  /**
   * Update nodes by id. All props of node are updated
   * @param {Object} node
   * @param {String} node.type node type person,product,organisation
   * @param {String} node.label short label
   * @param {String} node.name node name
   * @param {Object} node.data all other data
   * @returns {Number} affected_rows number of row are affected by update
   */
  updateNodeById: node => {
    return execGraphQLQuery(gql.updateNodeById(node)).then(
      res => {
        if (res.errors) {
          return res
        } else {
          let {
            update_nodes: { affected_rows },
            update_nodes: {
              returning: {
                0: { dataB64 }
              }
            }
          } = res
          return { affected_rows, dataB64 }
        }
      }
    )
  },
  deleteOnlyNodeById: id => {
    return execGraphQLQuery(gql.deleteOnlyNodeById(id)).then(
      res => {
        if (res.errors) {
          return res
        } else {
          let {
            delete_nodes: { affected_rows }
          } = res
          return { affected_rows }
        }
      }
    )
  },
  deleteLinksFromNode: node_id => {
    return execGraphQLQuery(
      gql.deleteLinksFromNode(node_id)
    ).then(res => {
      if (res.errors) {
        return res
      } else {
        let {
          delete_links: { affected_rows }
        } = res
        return { affected_rows }
      }
    })
  },
  /**
   * Add link to collection of nodes
   * @param {Object} node
   * @param {String} node.type node type person,product,organisation
   * @param {String} node.label short label
   * @param {String} node.name node name
   * @param {Object} node.data all other data
   * @returns {String} node id
   */
  addLink: link => {
    return execGraphQLQuery(gql.addLink(link)).then(res => {
      if (res.errors) {
        return res
      } else {
        let {
          insert_links: {
            returning: {
              0: { id, dataB64 }
            }
          }
        } = res
        return { id, dataB64 }
      }
    })
  },
  deleteLinkById: id => {
    return execGraphQLQuery(gql.deleteLinkById(id)).then(
      res => {
        if (res.errors) {
          return res
        } else {
          let {
            delete_links: { affected_rows }
          } = res
          return { affected_rows }
        }
      }
    )
  },
  deleteNodeAndLinksByNodeId: id => {
    return execGraphQLQuery(gql.deleteNodeAndLinksByNodeId(id))
  }
}

export default graphqlService
