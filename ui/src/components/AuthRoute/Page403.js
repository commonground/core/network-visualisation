import React from 'react'
import IconSlot from '../../styles/icons/slot.svg'
import './Page403.scss'
/**
 * Based on error pages idea from Greg Shuster
 * for more info see: https://dribbble.com/shots/3023730-Sketch-Error-Pages
 */
const Page403 = () => {
  return (
    <section className="Page403">
      <h1 className="Page403-title">403 Forbidden</h1>
      <div className="Page403-image">
        <span>4</span>
        <img src={IconSlot} alt="forbidden" />
        <span>3</span>
      </div>
      <div className="Page403-message">
        Try <a href="/admin">again</a> or go to{' '}
        <a href="/">home page</a>
      </div>
    </section>
  )
}

export default Page403
