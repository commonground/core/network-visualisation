/**
 * Authentication service
 */

import jwt_decode from 'jwt-decode'
import { LS_TOKEN_KEY } from '../ForceDirectedGraph/Helpers/cg.types'

let authorizeUrl = '/auth'
let access_token_key = LS_TOKEN_KEY

if (process.env.REACT_APP_AUTH) {
  //console.log('auth endpoint...', process.env.REACT_APP_AUTH)
  authorizeUrl = process.env.REACT_APP_AUTH
}

function saveSessionToken(type, token) {
  if (sessionStorage && typeof token !== 'undefined') {
    sessionStorage.setItem(type, token)
    return true
  } else {
    return false
  }
}

function getSessionToken(key) {
  if (sessionStorage) {
    let token = sessionStorage.getItem(key)
    return token
  } else {
    throw Error('SessionStorage not supported')
  }
}

function deleteSessionToken(key) {
  if (sessionStorage) {
    sessionStorage.removeItem(key)
    return true
  } else {
    throw Error('SessionStorage not supported')
  }
}
/**
 * Validate if user token stored under key access_token_key
 * has a specific role.
 * @param {String} role user roles user or admin
 * @returns {Promise<boolean>} true/false promise
 */
function asyncTokenContainsUserRole(role) {
  return new Promise((res, rej) => {
    try {
      let token = getSessionToken(access_token_key)
      //check if token exist
      if (typeof token === 'undefined' || token === null) {
        res(false)
      } else {
        //validate token with auth api
        validateTokenAndUserRole(token, role).then(respose => {
          res(respose)
        })
      }
    } catch (e) {
      rej(e)
    }
  })
}
/**
 * Validate if user token is valid and has specific role
 * @param {String} token JWT to validate
 * @param {String} role role to validate: user or admin
 * @returns {Promise<boolean>} true/false promise
 */
function validateTokenAndUserRole(token, role) {
  let url = authorizeUrl + `/api/${role}`
  //return new Promise((res, rej) => {
  let headers = new Headers()
  headers.append('Content-Type', 'application/json')
  headers.append('Authorization', `Bearer ${token}`)
  return fetch(url, {
    method: 'GET',
    headers: headers
  })
    .then(res => {
      if (res.status === 200) {
        return true
      } else {
        return false
      }
    })
    .catch(e => {
      throw e
    })
}

/**
 * Validate if user token stored under key access_token_key
 * has a specific role.
 * @param {String} role user roles user or admin
 * @returns {Boolean} true/false
 */
function tokenContainsUserRole(role) {
  try {
    let token = getSessionToken(access_token_key)
    //check if token exist
    if (typeof token === 'undefined' || token === null)
      return false
    //decode token
    let decoded = jwt_decode(token)
    //extract user
    let { user } = decoded
    if (user) {
      if (user.roles.indexOf(role) > -1) {
        return true
      } else {
        return false
      }
    }
  } catch (e) {
    console.error('AuthSvc: ', e)
    return false
  }
}
/**
 * Remove access token from sessionStorage
 */
function logOut() {
  return deleteSessionToken(access_token_key)
}

function signUserToken() {
  let url = authorizeUrl + '/sign/user'
  let user = {
    name: 'John Doe',
    roles: ['user']
  }
  return fetchToken(url, user)
}

function signIn(user) {
  let url = authorizeUrl + '/signin'
  //add roles
  user['roles'] = ['user', 'admin']
  return fetchToken(url, user)
}

function fetchToken(url, user) {
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  })
    .then(
      res => {
        if (res.status === 200) {
          return res.json()
        } else {
          throw Error(`${res.status} ${res.statusText}`)
        }
      },
      e => {
        throw Error(e)
      }
    )
    .then(token => {
      const { access_token } = token
      return saveSessionToken(access_token_key, access_token)
    })
    .then(saved => {
      if (saved) {
        return true
      } else {
        throw Error('Failed to sign token')
      }
    })
}

function getHasuraRequestHeaders(hasuraAdmin = '') {
  let jwt = getSessionToken(LS_TOKEN_KEY)
  let headers = new Headers()
  headers.append('Content-Type', 'application/json')
  if (jwt) {
    headers.append('Authorization', `Bearer ${jwt}`)
  } else if (hasuraAdmin !== '') {
    headers.append('x-hasura-admin-secret', hasuraAdmin)
  }
  return headers
}

export default {
  saveSessionToken,
  getSessionToken,
  deleteSessionToken,
  tokenContainsUserRole,
  asyncTokenContainsUserRole,
  validateTokenAndUserRole,
  getHasuraRequestHeaders,
  signUserToken,
  signIn,
  logOut
}
