import React, { Component } from 'react'

import { Route } from 'react-router-dom'

import AuthSvc from './AuthSvc'
import Page403 from './Page403'
import LoaderModal from '../ForceDirectedGraph/Modals/LoaderModal'
import LoginModal from '../ForceDirectedGraph/Modals/LoginModal'

import * as actionType from '../ForceDirectedGraph/Helpers/cg.types'

class AuthRoute extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      dataRole: props['data-role'],
      showLogin: true,
      authenticated: false
    }
  }
  /**
   * This function will create JWT token
   * with user role. The user will be able to view
   * Network page
   */
  hasUserToken() {
    AuthSvc.asyncTokenContainsUserRole('user')
      .then(res => {
        if (res === true) {
          return true
        } else {
          /**
           * NOTE! Creates user token on the fly
           * if the user does not have token.
           * Remove this if user roles also
           * need to login to app
           */
          return AuthSvc.signUserToken()
        }
      })
      .then(signed => {
        if (signed) {
          this.accessGranted()
        } else {
          throw new Error('Failed to sign')
        }
      })
      .catch(e => {
        //failed to create user token
        //show login screen
        this.showLoginModal()
      })
  }
  hasAdminToken() {
    AuthSvc.asyncTokenContainsUserRole('admin')
      .then(res => {
        if (res === true) {
          this.accessGranted()
        } else {
          this.showLoginModal()
        }
      })
      .catch(e => {
        this.show403page()
      })
  }
  authenticate(user) {
    AuthSvc.signIn(user)
      .then(signed => {
        if (signed) {
          this.accessGranted()
        } else {
          throw new Error('Failed to sign in')
        }
      })
      .catch(e => {
        console.error('Failed to sign in: ', e)
        this.show403page()
      })
  }
  handleModalAction = action => {
    //console.log('handle modal action...', action)
    switch (action.type) {
      case actionType.CANCEL_ACTION:
        this.show403page()
        break
      case actionType.SIGN_IN:
        this.authenticate(action.payload)
        break
      default:
        console.warn(
          `AuthRoute...actionType [${
            action.type
          }] not supported`
        )
    }
  }
  setDataRole = dataRole => {
    setTimeout(() => {
      this.setState({
        loading: true,
        authenticated: false,
        dataRole
      })
    }, 10)
  }
  showLoginModal() {
    this.setState({
      loading: false,
      authenticated: false,
      showLogin: true
    })
  }
  show403page() {
    this.setState({
      loading: false,
      authenticated: false,
      showLogin: false
    })
  }
  accessGranted() {
    this.setState({
      loading: false,
      authenticated: true,
      showLogin: false
    })
  }
  render() {
    const {
      loading,
      authenticated,
      dataRole,
      showLogin
    } = this.state
    const newRole = this.props['data-role']
    // debugger
    if (newRole !== dataRole) {
      this.setDataRole(newRole)
      return <LoaderModal title="Validating..." />
    } else if (dataRole === 'admin' && loading === true) {
      this.hasAdminToken()
    } else if (dataRole === 'user' && loading === true) {
      this.hasUserToken()
    }
    if (loading) {
      return <LoaderModal title="Validating..." />
    } else if (showLogin === true) {
      return (
        <LoginModal onModalAction={this.handleModalAction} />
      )
    }
    if (authenticated) {
      return <Route {...this.props} />
    } else {
      return <Page403 />
    }
  }
}

export default AuthRoute
