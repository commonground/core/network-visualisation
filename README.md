# Common Ground Network Visualisation

The repository contains application for visualizing VNG Common Ground Community.
It shows network of edges and nodes and enables editing from the UI.

## Development

The front-end is developed with React and D3.js. The back-end uses Hasura GraphQL and PostgreSQL. For more specific information about front-end see [readme file in ui folder](./ui/README.md). For more information about Hasura api see [readme file in api folder](./api/README.md).

### Running development on local machine with Minikube, Helm and Skaffold

First ensure you have all required software installed

- [Docker](https://docs.docker.com/install/)
- [Hypervisor](https://kubernetes.io/docs/tasks/tools/install-minikube/#install-a-hypervisor)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Minkube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [Helm](https://helm.sh/docs/using_helm/#quickstart)
- [Skaffold](https://skaffold.dev/docs/getting-started/)

#### Setup / Preparation scripts

The setup script should be runned once per minikube instance. After the scripts are executed confirm that traefik is running in Minikube.

```bash
  # create minikube using VirtualBox vm-driver
  # if you use different hypervisor adapt vm-driver param
  minikube start --vm-driver=VirtualBox --cpus 4 --memory 8192 --disk-size=50G
  # install Traefik ingress controller if not installed. Note! This should be done once per minikube installation
  helm install stable/traefik --name traefik --namespace traefik --values helm/traefik-values-minikube.yaml
  #confirm traefik is running in its namespace
  kubectl get pods --namespace traefik
```

When developing locally domain name cgn.minikube need to be defined in your local hosts file. Depending on OS the location might differ. The code below handles Linux Ununtu/Mint location

```bash
  # get minikube ip
  minikube ip

  # open hosts file as sudo user
  # with XED editor (sidenote: use any text editor)
  sudo xed /etc/hosts

  # add CGN domains to hosts file having
  # minikube IP you retreived at previous step

  # {Minikube IP} ui.cgn.minikube
  # {Minikube IP} hasura.cgn.minikube

```

Front end can be viewed on http://ui.cgn.minikube:30080/ or https://ui.cgn.minikube:30443/

#### Development with skaffold

Skaffold will autmatically watch for the changes and rebuild containers on each change. To start developing using skaffold

```bash
  # start minikube IF NOT already started
  minikube start
  # start cg-netrwork-visualization in dev mode
  skaffold dev --profile minikube
  # use Ctrl + C to stop development session
```

To stop development

```bash
  # use Ctrl + C to stop skaffold terminal
  # THEN
  # stop minikube
  minikube stop
```

#### Build solution with skaffold

```bash
  # builds the solution using minikube profile
  skaffold run --profile minikube

  # build with production profile is defined in .gitlab-ci.yml
```

### Running local instance with docker-compose

Required software

- [Docker](https://docs.docker.com/install/)
- [Docker compose](https://docs.docker.com/compose/install/)

If you have Docker and docker-compose installed you can run api portion of cg-network-visualisation using docker-compose and frontend using npm. This is preffered approach when developing frontend features. For more information [see readme file in ui folder](/ui/README.md)

## Deployment

Deployment is done using Gitlab CI/CD. Deployment steps are defined in `.gitlab-ci.yml` file.

### CI deployment to Gitlab pages

The code pushed to branch `fe-gitlab-pages` is automatically deployed to [gitlab page](https://commonground.gitlab.io/cg-network-visualisation/). This version uses Hasura test backend on Heroku.

The code pushed to `master` branch is automatically deployed on Common Ground Bravo cluster.

### Helm chart

Note! If for any reason helm deployment fails you need to remove previous deployment manually using `helm del <deployment name> --purge`. To check for any leftovers use `helm ls --all`.

```bash
  # install cgn-dev to cgn-dev namespace using helm and passing secrets
  helm install helm/cgn -n cgn-dev --namespace cgn-dev -f helm/cgn/val-dev.yaml --set apiAuthKey="AuthKey123456789" --set apiAdminName="secretAdmin" --set apiAdminPass="secretPass" --set apiHasuraAdminSecret="hasuraAdminSecret" --debug

  # upgrade cgn-dev or install
  helm upgrade --install cgn-dev ./helm/cgn --namespace cgn-dev -f helm/cgn/val-dev.yaml --set apiAuthKey="AuthKey123456789" --set apiAdminName="secretAdmin" --set apiAdminPass="secretPass" --set apiHasuraAdminSecret="hasuraAdminSecret" --debug

  # install cgn-prod to cgn-prod namespace using helm and passing secrets
  helm install helm/cgn -n cgn-prod --namespace cgn-prod -f helm/cgn/val-prod.yaml --set apiAuthKey=$API_AUTH_KEY --set apiAdminName=$API_ADMIN_NAME --set apiAdminPass=$API_ADMIN_PASS --set apiHasuraAdminSecret=$API_HASURA_ADMIN_NAME --debug

  # upgrade cgn-prod
  helm upgrade --install cgn-prod ./helm/cgn --namespace cgn-prod -f helm/cgn/val-prod.yaml --set apiAuthKey=$API_AUTH_KEY --set apiAdminName=$API_ADMIN_NAME --set apiAdminPass=$API_ADMIN_PASS --set apiHasuraAdminSecret=$API_HASURA_ADMIN_NAME --debug

```

## Licence

Copyright © VNG Realisatie 2017
[Licensed under the EUPL](LICENCE.md)
